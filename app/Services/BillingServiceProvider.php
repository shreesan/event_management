<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller

use App\Event;
use App\EventType;
use App\BookedService;
use App\ServiceItem;
use App\Merchant;

use PDF; //To use DomPDF

/*
|--------------------------------------------------------------------------
|  Billing Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for generating and downloading Bills for Events. 
|
*/
class BillingServiceProvider
{
    /**
     * Generate the Event Bill.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function generateBill($input)
    {
        $id = $input['booking_id'];

        $caterer = config('constants.caterer');
        $onlineTransfer = config('constants.onlineTransfer');

        Event::where('booking_id', $id)
            ->update([
                'bill_status' => '1', // Update in the database that the bill has been generated.
            ]);

        $payment_status = Event::where('booking_id', $id)->pluck('payment_mode_id')->first();

        if ($payment_status != $onlineTransfer) {
            Event::where('booking_id', $id)
            ->update([
                'payment_status' => '1', // Update in the database that the amount has been paid in either cash/cheque.
            ]);
        }
        
        $allServices = BookedService::where('events_booking_id', $id)->get();

        /* Make all the services which are not accepted, as rejected. Hence no billing for them. */
        $services = $allServices->pluck('status');
        foreach ($services as $service) {
            if ($service == 0 ) {
                BookedService::where([
                    ['events_booking_id', $id],
                    ['status', $service],
                ])->update([
                        'status' => '2',
                    ]);
            }
        }

        $total_cost = 0; // Variable to hold the bill_amount.

        $final_Services = BookedService::where([
                    ['events_booking_id', $id],
                    ['status', 1],
                ])->get()->pluck('service_id');

        $no_of_guests = Event::where('booking_id', $id)->get()->sum('no_of_guests');

        /* Calculation of the bill amount is done as follows : */
        foreach ($final_Services as $final_Service) {
            
            $service = ServiceItem::where('id', $final_Service)->get()->pluck('merchant_id');
            $merchant_type = Merchant::where('id', $service)->get()->sum('merchant_type_id');

            $a = ServiceItem::where('id', $final_Service)->sum('cost');
            
            /* Type '1' merchant = Caterer. We need to multiply the cost of each food item with the number of guests, and hence add to other services costs, to get the total event cost. */
            if ($merchant_type == $caterer) {
                $total_cost = $total_cost + ($a * $no_of_guests);
            }
            else{
                $total_cost = $total_cost + $a;
            }
        }
        
        $eventBilled = Event::where('booking_id', $id)
            ->update([
                'bill_amount' => $total_cost,
            ]);
        
        if ($eventBilled) {
            return back()->with('success','Bill Generated Successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');

    }

    /**
     * Download bill for Confirmed event.
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadPDF($input)
    {
        $id = $input['booking_id'];
        $events = Event::where('booking_id', $id)->get();

        $eventType = $events->pluck('event_type_id');
        $eventName = EventType::where('id', $eventType)->get()->pluck('event_type')->first();

        $carts = BookedService::all();

        /* Created an empty array to store the billable events only. It is a big error in DomPDF to send entire array (also consisting of non required rows) -> DomPDF throws error "Row #no. not found".*/

        $myCart = array();

        foreach ($carts as $cart) {
            if (($cart->events_booking_id == $id) && ($cart->status == '1')) {
                $myCart[] = $cart;
            }
        }

        /* Send the data to DomPDF's loadView() by using php compact(). */
        $pdf = PDF::loadView('events.bill', compact('events', 'myCart'));
        /* Set the PDF format as landscape, as the table is longer in length. Set paper size as A4. */
        $pdf->setPaper('A4', 'landscape');
        /* Download the bill in the name of Event. */
        return $pdf->download($eventName.'_bill.pdf');

    }

    /**
     * Show the bill page.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function showBill(Request $request)
    {
        $events = Event::where('booking_id', '5cc05b5cb5ee9')->get();
        $carts = BookedService::all();

        $myCart = array();

        foreach ($carts as $cart) {
            if( ($cart->events_booking_id == '5cc05b5cb5ee9') && ($cart->status == '1')) {
                $myCart[] = $cart;
            }
        }

        return view('events/bill', ['events'=>$events], ['myCart'=>$myCart]);
    }*/
}