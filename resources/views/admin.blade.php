@extends('layouts.mylayout')

@section('navigation')

@endsection

@section('sidebar')
  @parent
    <li class="active">
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>
    
    <li class="header">
      <i class="fa fa-folder-open"></i> RESOURCES
    </li>

	  <li>
	  	<a href="{{ url('merchantDetails') }}">
	  		<i class="fa fa-user"></i> 
	  		<span> View Merchant Details</span>
	  	</a>
	  </li>

	  <li>
	  	<a href="{{ url('bookedEvents') }}">
	  		<i class="fa fa-pencil-square-o"></i> 
	  		<span> View Booked Events</span>
	  	</a>
	  </li>

@endsection

@section('content')
  <!-- The Admin gets to see, edit, and delete users' details here. -->
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>Manage <b>Users</b></h2>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>User Type</th>
          <th>Email</th>
          <th>Address</th>
          <th>Phone</th>
          <th>Actions</th>
        </tr>
      </thead>

      <tbody>
      	@foreach ($users as $user)
	        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>

            <td>{{ $user->user_type }}</td>

            <td>{{ $user->email }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->contact_no }}</td>
            <td>
              <a href="#modal-edit-{{ $user->id }}" class="edit" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
              </a>

              <!-- Edit User Modal -->
              <div class="modal fade" id="modal-edit-{{ $user->id }}">
                <form method="POST" action="{{ url('/home') }}">
                  {{ csrf_field() }}
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center">Edit User Data</h4>
                      </div>
                      <div class="modal-body">
                        <div class="login-box-body row">
                          <div class="col-xs-1"></div>

                          <div class="col-xs-10">
                            <div class="form-group">
                              <input id="operation" type="hidden" class="form-control" name="operation" value="editUser" required>
                            </div>

                            <div class="form-group">
                              <input id="id" type="hidden" class="form-control" name="id" value="{{ $user->id }}" required>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
                              <label for="name">Enter Full name: </label>
                              <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Full name" required autofocus>
                              <span class="glyphicon glyphicon-user form-control-feedback"></span>

                              @if ($errors->has('name'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
                              <label for="email">Enter email: </label>
                              <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}" placeholder="Email" required>
                              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

                              @if ($errors->has('email'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} has-feedback">
                              <label for="address">Enter Address: </label>
                              <textarea id="address" type="text" class="form-control" name="address" rows="2" placeholder="Personal Address" required>{{ $user->address }}</textarea>
                              <span class="glyphicon glyphicon-home form-control-feedback"></span>

                              @if ($errors->has('address'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('address') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }} has-feedback">
                              <label for="contact_no">Enter Contact Number: </label>
                              <input id="contact_no" type="text" class="form-control" name="contact_no" value="{{ $user->contact_no }}" placeholder="Contact Number" required>
                              <span class="glyphicon glyphicon-phone form-control-feedback"></span>

                              @if ($errors->has('contact_no'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('contact_no') }}</strong>
                                </span>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary form-group">Update</button>
                      </div>

                    </div>
                  </div>
                </form>  
              </div>

              <a href="javascript:void(0);" class="delete" onclick="admin.deleteUser(this);" data-id="{{ $user->id }}" data-name="{{ $user->name }}">
                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
              </a>
              
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
