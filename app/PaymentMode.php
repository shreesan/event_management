<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMode extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Payment Mode Type Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "payment_mode" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'payment_mode';

    /**
     * Get the Events for the PaymentMode.
     * The Foreign key names are overridden.
     */
    public function eventByPayType()
    {
        return $this->hasMany('App\Event', 'payment_mode_id');
    }
}
