<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use DB;
use Debugbar;

use App\Merchant;
use App\ServiceItem;
use App\Http\Controllers\UiController;

/*
|--------------------------------------------------------------------------
|  Merchant Draw Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for drawing the Modal and its bodies for all 
| modals used while in merchant login. 
|
*/
class MerchantDrawServiceProvider
{
	/**
   * Function to draw the required Modal for confirmed services.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
	static function makeConfirmServiceModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => 'modal-lg',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawConfirmServiceModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for confirmed services.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeConfirmServiceModalBody($params)
  {

    $event_id = $params['event_id'];

    $currentMerchant = Merchant::where('user_id', Auth::user()->id)->pluck('id')->first();

    $myCarts = DB::table('booked_services')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            
            ->where('service_items.merchant_id', '=', $currentMerchant)
            ->where('booked_services.events_booking_id', '=', $event_id)
            ->where('booked_services.status', '=', 1)
            
            ->select(
                      'service_items.item',
                      'service_items.cost',
                      'service_items.description'
                    )
            ->get();
      
    return view('modalBody/confirmServiceModalBody', ['myCarts'=>$myCarts]);
  }

  /**
   * Function to draw the required Modal for ADD service.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeAddServiceModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawServiceAddFormModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for form of ADD services.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeAddServiceModalBody($params)
  {  
    return view('modalBody/serviceAddFormModalBody');
  }



  /**
   * Function to draw the required Modal for EDIT service details.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeEditServiceModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawServiceEditFormModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for form of EDIT services.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeEditServiceModalBody($params)
  {

    $service_id = $params['item_id'];

    $item = DB::table('service_items')
            ->where('id', '=', $service_id)
            
            ->select(
                      'service_items.id',
                      'service_items.item',
                      'service_items.cost',
                      'service_items.description',
                      'service_items.image'
                    )
            ->get();
    // Debugbar::info($item);
      
    return view('modalBody/serviceEditFormModalBody', ['item'=>$item]);
  }

  /**
   * Function to draw the required Modal for Delete service details.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeDeleteServiceModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawServiceDeleteFormModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for form of Delete services.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeDeleteServiceModalBody($params)
  {

    $service_id = $params['item_id'];

    $item = DB::table('service_items')
            ->where('id', '=', $service_id)            
            ->select(
                      'service_items.id',
                      'service_items.item'
                    )
            ->get();
    // $item = ServiceItem::where('id', $service_id)->get();
      
    return view('modalBody/serviceDeleteFormModalBody', ['item'=>$item]);
  }


  //accept

  /**
   * Function to draw the required Modal for accept requests form.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeAcceptRequestModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawAcceptRequestFormModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for accept requests form.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeAcceptRequestModalBody($params)
  {
    $service_id = $params['cart_id'];

    $cart = DB::table('booked_services')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            ->join('events', 'booked_services.events_booking_id', '=', 'events.booking_id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
            
            ->where('booked_services.id', '=', $service_id)
            
            ->select(
                      'booked_services.id',
                      'service_items.item',
                      'events.date',
                      'event_type.event_type'
                    )
            ->get();

    return view('modalBody/acceptRequestFormModalBody', ['cart'=>$cart]);
  }

  //reject

  /**
   * Function to draw the required Modal for reject requests form.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeRejectRequestModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawRejectRequestFormModalBody','MerchantController',$params,$options);

    return $data;
  }

  /**
   * Function to draw the Modal body for reject requests form.
   *
   * @param  \App\Merchant  $params
   * @return \Illuminate\Http\Response
   */
  static function makeRejectRequestModalBody($params)
  {
    $service_id = $params['cart_id'];

    $cart = DB::table('booked_services')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            ->join('events', 'booked_services.events_booking_id', '=', 'events.booking_id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
            
            ->where('booked_services.id', '=', $service_id)
            
            ->select(
                      'booked_services.id',
                      'service_items.item',
                      'events.date',
                      'event_type.event_type'
                    )
            ->get();

    return view('modalBody/rejectRequestFormModalBody', ['cart'=>$cart]);
  }

}