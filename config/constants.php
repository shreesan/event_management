<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Static Values and Constants used in Project
    |--------------------------------------------------------------------------
    | Remember to run : php artisan config:clear - After any changes to config.
    */

    'timeZone' => 'Asia/Kolkata', // timeZone used to insert event into Google Calendar
    'stripeKey' => 'sk_test_O8zvHNuO5WCVSqr7phCpqLNX00fT3nR54S', // Stripe API key
    'currency' => 'INR', // Currency used for API payments
    'AJAX_URL' => '/ajax', //The url where all AJAX Calls will hit

    //Type of Users :
    'admin' => 1, //User type = Admin
    'host' => 2, //User type = Host
    'merchant' => 3, //User type = Merchant

    //Type of Merchants :
    'caterer' => 1, //Merchant type = Caterer
    'hallOwner' => 2, //Merchant type = Hall Owner
    'decorator' => 3, //Merchant type = Decorator
    'florist' => 4, //Merchant type = Florist
    'pandit' => 5, //Merchant type = Pandit
    'baker' => 6, //Merchant type = Baker

    //Type of Events :
    'marriage' => 1, //Event type = Marriage
    'birthday' => 2, //Event type = Birthday
    'anniversary' => 3, //Event type = Anniversary
    'grihapravesh' => 4, //Event type = Grihapravesh
    'threadCeremony' => 5, //Event type = Thread ceremony
    'annaprasanna' => 6, //Event type = Annaprasanna
    '21stDay' => 7, //Event type = 21st Day Ceremony

    //Types of Payment :
    'cash' => 1, //Payment type = Cash
    'cheque' => 2, //Payment type = Cheque
    'onlineTransfer' => 3, //Payment type = Online Transfer

];
