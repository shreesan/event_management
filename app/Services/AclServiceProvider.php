<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\User;
use App\UserType;
use App\Merchant;

//to show the final booked events to merchants :
use App\Event;
use App\EventType;
use App\PaymentMode;
use App\BookedService;

// For Google API usage (Calendar) :
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;

use DB;

/*
|--------------------------------------------------------------------------
|  ACL Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for viewing respective Homepages as per ACL 
| to the three types of users - Hosts, Merchants and Admin.
|
*/
class AclServiceProvider
{
	/**
     * Function to show for all User data only to admin.
     *
     * @param string $view
     * @return \Illuminate\Http\Response
     */
    private function viewAdmin($request, $view)
    {
        $users = DB::table('users')
            ->join('user_type', 'users.user_type_id', '=', 'user_type.id')
            
            ->select(
                      'users.id',
                      'users.name',
                      'users.email',
                      'users.address',
                      'users.contact_no',
                      'user_type.user_type'
                    )
            ->get();

        return view($view, ['users'=>$users]);
    }

    /**
     * Function to show merchant the confirmed events which he needs to serve.
     *
     * @param string $view
     * @return \Illuminate\Http\Response
     */
    private function viewMerchant($request, $view)
    {
        $currentMerchant = Merchant::where('user_id', Auth::user()->id)->pluck('id')->first();

        $myCarts = DB::table('booked_services')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            
            ->where('service_items.merchant_id', '=', $currentMerchant)
            ->where('booked_services.status', '=', 1)
            
            ->select(
                        'booked_services.events_booking_id'
                    )
            ->pluck('events_booking_id')->unique();

        $myEvents = DB::table('events')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
            ->join('payment_mode', 'events.payment_mode_id', '=', 'payment_mode.id')

            ->where('events.bill_status', '=', 1)
            ->whereIn('events.booking_id', $myCarts)

            ->select(
                        'events.booking_id',
                        'users.name', 
                        'event_type.event_type',
                        'payment_mode.payment_mode',
                        'events.date', 
                        'events.no_of_guests',
                        'events.is_complete'
                    )

            ->get()
            ->unique();

        return view($view, ['myEvents'=>$myEvents]);
    }

    /**
     * Function to creates auth URL to get Google Auth code and store it in session.
     *
     * @return Google Auth URL.
     */
    private function createGoogleAuthUrl()
    {
        $client = new Google_Client();
        $client->setScopes(Google_Service_Calendar::CALENDAR);
        $client->setAuthConfig('client_secret.json');
        $authUrl = $client->createAuthUrl();

        return $authUrl;
    }

    /**
     * Function to show Host Homepage.
     *
     * @param string $view
     * @return \Illuminate\Http\Response
     */
    private function viewHost($request, $view)
    {
        $authUrl = $this->createGoogleAuthUrl();
        $request->session()->put('googleCalendarAuthUrl',$authUrl);

        return view($view);
    }

    /**
     * Function to show the application Homepage to different Users.
     *
     * @return \Illuminate\Http\Response
     */
	public function Acl($request)
	{
		$user = Auth::user()->user_type_id;
        
        $admin = config('constants.admin');
        $host = config('constants.host');
        // $merchant = config('constants.merchant');

        // show for the Hosts.
        if ($user === $host) {
            return $this->viewHost($request, 'home');
        }
        // show for the Admin.
        elseif ($user === $admin) {
            return $this->viewAdmin($request, 'admin');
        }
        // show for the Merchants.
        else {
            return $this->viewMerchant($request, 'merchant');
        }
	}
}