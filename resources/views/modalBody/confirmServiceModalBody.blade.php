<div class="login-box-body row">
	<div class="table-wrapper">
		<div class="table-title">
			<div class="row">
				<div class="col-sm-6">
					<h2>Confirmed <b>Services</b></h2>
				</div>
			</div>
		</div>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th class="col-md-4">Service</th>
					<th class="col-md-2">Cost</th>
					<th class="col-md-6">Description</th>
				</tr>
			</thead>

			<tbody>
				@foreach ($myCarts as $cart)
					<tr>
						<td>{{ $cart->item }}</td>
						<td>&#8377 {{ $cart->cost }} /-</td>
						<td>{{ $cart->description }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>