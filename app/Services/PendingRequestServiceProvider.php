<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use DB;

use App\Merchant;
use App\BookedService;

/*
|--------------------------------------------------------------------------
|  Pending Requests Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all with the showing and approval of 
| pending requests by Merchants. 
|
*/
class PendingRequestServiceProvider
{
	/**
   * Show the Pending Requests for the merchant.
   *
   * @return \Illuminate\Http\Response
   */
  public function myPendingRequests($request)
  {
    $currentMerchant = Merchant::where('user_id', Auth::user()->id)->pluck('id')->first();

    $carts = DB::table('booked_services')
        ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
        ->join('events', 'booked_services.events_booking_id', '=', 'events.booking_id')

        ->join('users', 'events.user_id', '=', 'users.id')
        ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
        ->join('payment_mode', 'events.payment_mode_id', '=', 'payment_mode.id')
        
        ->where('service_items.merchant_id', '=', $currentMerchant)
        
        ->select(
                  'booked_services.id',
                  'booked_services.status',
                  'service_items.item',
                  'events.date', 
                  'events.no_of_guests',
                  'users.name',
                  'users.email',
                  'users.contact_no',
                  'event_type.event_type',
                  'payment_mode.payment_mode'
                )
        ->get();

    return view('services/pendingRequests', ['carts'=>$carts]);
  }

  /**
   * Function of approval or rejection of Booked services.
   *
   * @return \Illuminate\Http\Response
   */
  public function requestApprovalRejection($input)
  {
    $id = $input['id'];

    /* Update the status of the booked service as per the action of the merchant. */
    if ($input['operation'] == "accept") {
      $serviceAccepted = BookedService::where('id', $id)->update(['status' => '1']);
      if ($serviceAccepted) {
        return back()->with('success','Event Accepted !');
      }
      return back()->with('error','Oops! Something Went Wrong.');
    }

    else {
      $serviceRejected = BookedService::where('id', $id)->update(['status' => '2']);
      if ($serviceRejected) {
        return back()->with('success','Event Rejected !');
      }
      return back()->with('error','Oops! Something Went Wrong.');      
    }
  }

}
