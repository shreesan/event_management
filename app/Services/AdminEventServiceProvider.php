<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use DB;
use Validator;

use App\Event;
use App\User;
use App\BookedService;

/*
|--------------------------------------------------------------------------
|  Admin Event Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all interactions with the Events by Admin. 
|
*/
class AdminEventServiceProvider
{
	/**
     * Shows event and services details to Admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEventDetails($request)
    {

        $carts = DB::table('booked_services')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            ->join('merchants', 'service_items.merchant_id', '=', 'merchants.id')
            ->join('users', 'merchants.user_id', '=', 'users.id')
            ->join('merchant_type', 'merchants.merchant_type_id', '=', 'merchant_type.id')
            
            ->select(
                        'booked_services.events_booking_id',
                        'booked_services.status',
                        'booked_services.id',
                        'service_items.item',
                        'service_items.cost',
                        'service_items.description',
                        'merchants.company_name',
                        'merchants.landline_no',
                        'merchants.merchant_type_id',
                        'merchant_type.merchant_type',
                        'users.contact_no',
                        'users.email'
                    )
            ->get();

        $eventsBooked = $carts->pluck('events_booking_id')->unique();

        $events = DB::table('events')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
            ->join('payment_mode', 'events.payment_mode_id', '=', 'payment_mode.id')

            ->whereIn('events.booking_id', $eventsBooked)

            ->select(
                        'events.id',
                        'events.booking_id',
                        'events.bill_status',
                        'events.event_type_id',
                        'events.payment_mode_id',
                        'users.name', 
                        'event_type.event_type',
                        'payment_mode.payment_mode',
                        'events.date',
                        'events.bill_amount',
                        'events.no_of_guests',
                        'events.is_complete'
                    )

            ->get()
            ->unique();
        
        return view('admin/bookedEvents', ['events'=>$events], ['carts'=>$carts]);
    }

	/**
     * Update the Event details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function updateEventDetail($input)
    {
    	$this->validator($input)->validate();

        $id = $input['booking_id'];

        Event::where('booking_id', $id)
            ->update([
                'event_type_id' => $input['event_type_id'],
                'payment_mode_id' => $input['payment_mode_id'],
                'no_of_guests' => $input['no_of_guests'],
                'date' => $input['date'],
                'bill_status' => '0',
            ]);

        $updatedEvent = BookedService::where('events_booking_id', $id)
            ->update([
                'status' => '0',
            ]); //Change the status of all booked services in cart to '0'(waiting for approval), when event details are updated.

        if ($updatedEvent) {
            return back()->with('success','Event Updated Successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Delete the Event details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function deleteEventDetail($input)
    {
        $id = $input['booking_id'];
        $deletedEvent = Event::where('booking_id', $id)->delete();
        
        if ($deletedEvent) {
            return back()->with('success','Event Details Deleted !');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Delete the Service details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function deleteServiceDetail($input)
    {
        $id = $input['id'];
        $deletedService = BookedService::where('id', $id)->delete();
        
        if ($deletedService) {
            return back()->with('success','Service Deleted successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }
	
	/**
	* Get a validator for update event request.
	*
	* @param  array  $data
	* @return \Illuminate\Contracts\Validation\Validator
	*/
	protected function validator(array $data)
	{
		return Validator::make($data, [
			'no_of_guests' => 'required|numeric|min:50',
		]);
	}

    /**
     * Shows venue Location to Admin.
     *
     * @return \Illuminate\Http\Response
     */
    public function showVenueLocation($request)
    {
        $method = $request->method();
        app('debugbar')->disable();
        
        if ($request->isMethod('post')) {
            $input = $request->all();        
            $location = $input['location'];
            $venueName = $input['venueName'];            
            return view('admin/venueLocationPage', ['location'=>$location], ['venueName'=>$venueName]);
        }
        else {
            abort(404,'Unauthorized Access !!!');
        }
    }

}
