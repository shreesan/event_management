@extends('layouts.mylayout')

@section('content')
	<!-- View the recieved error message (warning_symbol= &#9888;) -->
	<div class="text-center">
		<h1>{{ $exception->getMessage() }}</h1>
		<h3>
			<a href="{{ url('/home') }}">
				<i class="fa fa-arrow-circle-left"></i>
        Go to Home
      </a>
		</h3>
	</div>
@endsection
