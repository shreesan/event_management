<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\Merchant;
use App\User;

use DB;
use Debugbar;
use Illuminate\Support\Facades\Log;

use App\Services\EventItemServiceProvider;
use App\Services\PendingRequestServiceProvider;
use App\Services\MerchantDrawServiceProvider;

class MerchantController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  |  Merchant Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for viewing respective pages as per ACL 
  | to the three types of users - Hosts, Merchants and Admin.
  |
  */

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  /**
   * Function to draw the Modal structure for my services.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawListView($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeConfirmServiceModal($params);
  }

  /**
   * Function to draw the Modal body for confirmed services.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawConfirmServiceModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeConfirmServiceModalBody($params);
  }

  /**
   * Function to draw the Modal structure for Add service.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceAddForm($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeAddServiceModal($params);
  }

  /**
   * Function to draw the Modal body for Add services form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceAddFormModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeAddServiceModalBody($params);
  }


  /**
   * Function to draw the Modal structure for edit service.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceEditForm($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeEditServiceModal($params);
  }

  /**
   * Function to draw the Modal body for edit services form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceEditFormModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeEditServiceModalBody($params);
  }

  /**
   * Function to draw the Modal structure for Delete service.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceDeleteForm($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeDeleteServiceModal($params);
  }

  /**
   * Function to draw the Modal body for Delete services form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawServiceDeleteFormModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeDeleteServiceModalBody($params);
  }

  //accept

  /**
   * Function to draw the Modal structure for accept request form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawAcceptRequestForm($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeAcceptRequestModal($params);
  }

  /**
   * Function to draw the Modal body for accept request form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawAcceptRequestFormModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeAcceptRequestModalBody($params);
  }

  //reject

  /**
   * Function to draw the Modal structure for reject request form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawRejectRequestForm($params = array())
  {
    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeRejectRequestModal($params);
  }

  /**
   * Function to draw the Modal body for reject request form.
   *
   * @param  \App\Merchant  $params = array()
   * @return \Illuminate\Http\Response
   */
  static function drawRejectRequestFormModalBody($params = array())
  {

    $drawModal = new MerchantDrawServiceProvider();
    return $drawModal->makeRejectRequestModalBody($params);
  }

  /**
   * Display the specified resource.
   *
   * @param  \App\Merchant  $merchant
   * @return \Illuminate\Http\Response
   */
  public function show(Merchant $merchant)
  {
    return $merchant;
  }
  

  /**
   * Function to check for Authentication of the merchant.
   *
   * @return \Illuminate\Http\Response
   */
  private function checkMerchant($request)
  {
    $merchant = config('constants.merchant');
    $user = Auth::user()->user_type_id;

    //Check for the Merchant.
    if ($user === $merchant) {
        return 1;
    }
    else {
        abort(404,'Unauthorized Access !!!');
    }
  }

  /**
   * Show the My Services page for the merchant.
   *
   * @return \Illuminate\Http\Response
   */
  public function services(Request $request)
  {
    if ($this->checkMerchant($request) == 1) {
      $eventService = new EventItemServiceProvider();
      return $eventService->merchantServices($request);
    }
  }

  /**
   * Show the Pending Requests page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showPendingRequests(Request $request)
  {
    if ($this->checkMerchant($request) == 1) {
      $pendingRequest = new PendingRequestServiceProvider();
      return $pendingRequest->myPendingRequests($request);
    }
  }

  /**
   * Function to approve / reject the incoming requests.
   *
   * @return \Illuminate\Http\Response
   */
  public function approveRequests(Request $request)
  {
    $input = $request->all();
    $pendingRequest = new PendingRequestServiceProvider();
    return $pendingRequest->requestApprovalRejection($input);
  }

  /**
   * To create, update and delete Services.
   *
   * @return \Illuminate\Http\Response
   */
  public function serviceDetails(Request $request)
  {
      $input = $request->all();
      $eventService = new EventItemServiceProvider();

      switch ($input['operation']) {
        case 'add':
          return $eventService->addServiceDetail($request, $input);
          break;

        case 'edit':
          return $eventService->updateServiceDetail($request, $input);
          break;

        case 'delete':
          return $eventService->deleteServiceDetail($input);
          break;
      }

  }

  

}
