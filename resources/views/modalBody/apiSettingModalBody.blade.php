@if (Auth::user()->google_auth_code == '0')
	<div class="text-center">
		<a href="{{ Session::get('googleCalendarAuthUrl') }}" target="_blank">
	    <button class="btn btn-primary">Click here to get your <strong>Authorization Code</strong> from Google</button>
	  </a>
	</div>

  <form method="POST" action="{{ url('/myEvents') }}">
    {{ csrf_field() }}

    <div class="form-group">
      <input id="operation" type="hidden" class="form-control" name="operation" value="api" required>
    </div>

    <div class="form-group">
      <input id="id" type="hidden" class="form-control" name="id" value="{{ Auth::user()->id }}" required>
    </div>

    <div class="form-group{{ $errors->has('auth_code') ? ' has-error' : '' }} has-feedback">
      <label for="auth_code">Enter Code: </label>
      <input id="auth_code" type="text" class="form-control" name="auth_code" placeholder="Paste the Authorization Code Here" required autofocus>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      @if ($errors->has('auth_code'))
        <span class="help-block">
          <strong>{{ $errors->first('auth_code') }}</strong>
        </span>
      @endif
    </div>

	  <div class="form-group pull-right">
	  	<button type="submit" class="btn btn-primary form-group">Submit</button>
	  </div>
	
	</form>

@else
  <h3>You Have Already Authenticated your Google Account</h3>
@endif