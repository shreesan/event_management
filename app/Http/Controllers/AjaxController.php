<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Debugbar;

class AjaxController extends Controller
{
   /**
     * Function to handle all AJAX requests.
     *
     * @return \Illuminate\Http\Response
     */
    public function processAjax(Request $request)
    {
    	$params 		= $request->all();
    	
    	Debugbar::info($params);
    	Debugbar::log('=== In processAjax ===');
    	
    	// I think we do not need to have a service for this
        // as ajax requests only deal with browsers

     	$className 		= (isset($params['className']) && $params['className'])?$params['className']:'';
		$functionName 	= (isset($params['functionName']) && $params['functionName'])?$params['functionName']:'';

		if(!$className || !$functionName){
			Debugbar::log("ERROR: Missing Parameters");
			return false;
		}

		// calling the intended method
		Debugbar::log('=== Calling To: '.$className.'::'.$functionName.' ===');
		
		$resp 			= call_user_func(array('App\Http\Controllers\\'.$className, $functionName),$params);

		return $resp;
    }
    
}
