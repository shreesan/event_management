<?php

use Faker\Generator as Faker;

$factory->define(App\Merchant::class, function (Faker $faker) {
    return [
        'company_name' => $this->faker->company,
        'company_address' => $this->faker->city,
        'landline_no' => mt_rand(1000000000, 9999999999),
    ];
});
