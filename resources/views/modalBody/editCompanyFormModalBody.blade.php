<form method="POST" action="{{ url('/home') }}">
	{{ csrf_field() }}
		<div class="form-group">
      <input id="operation" type="hidden" class="form-control" name="operation" value="editCompany" required>
    </div>

    <div class="form-group">
      <input id="id" type="hidden" class="form-control" name="id" value="{{ Auth::user()->hasMerchants->first()->id }}" required>
    </div>

    <div class="form-group">
      <label for="merchant_type_id">Select Merchant Type: </label>
      <select class="form-control" name="merchant_type_id" id="merchant_type_id">

        <option value="1" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 1 ? 'selected' : '' }}>Caterer</option>

        <option value="2" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 2 ? 'selected' : '' }}>Hall Owner</option>

        <option value="3" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 3 ? 'selected' : '' }}>Decorator</option>

        <option value="4" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 4 ? 'selected' : '' }}>Florist</option>

        <option value="5" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 5 ? 'selected' : '' }}>Pandit</option>

        <option value="6" required {{ old("merchant_type_id", Auth::user()->hasMerchants->first()->merchant_type_id) == 6 ? 'selected' : '' }}>Baker</option>

      </select>
    </div>

    <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }} has-feedback">
      <label for="company_name">Enter Company Name: </label>
      <input id="company_name" type="text" class="form-control" name="company_name" value="{{ Auth::user()->hasMerchants->first()->company_name }}" placeholder="Company Name" required autofocus>
      <span class="glyphicon glyphicon-globe form-control-feedback"></span>

      @if ($errors->has('company_name'))
        <span class="help-block">
          <strong>{{ $errors->first('company_name') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }} has-feedback">
      <label for="company_address">Enter Company Address: </label>
      <textarea id="company_address" type="text" class="form-control" name="company_address" rows="2" placeholder="Company Address" required>{{ Auth::user()->hasMerchants->first()->company_address }}</textarea>
      <span class="glyphicon glyphicon-pushpin form-control-feedback"></span>

      @if ($errors->has('company_address'))
        <span class="help-block">
          <strong>{{ $errors->first('company_address') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('landline_no') ? ' has-error' : '' }} has-feedback">
      <label for="landline_no">Enter Landline Number: </label>
      <input id="landline_no" type="text" class="form-control" name="landline_no" value="{{ Auth::user()->hasMerchants->first()->landline_no }}" placeholder="Landline Number" required>
      <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>

      @if ($errors->has('landline_no'))
        <span class="help-block">
          <strong>{{ $errors->first('landline_no') }}</strong>
        </span>
      @endif
    </div>

  <div class="form-group pull-right">
  	<button type="submit" class="btn btn-primary form-group">Update</button>
  </div>

</form>