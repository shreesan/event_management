<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use DB;

use Validator;

use App\User;
use App\Merchant;

/*
|--------------------------------------------------------------------------
|  Merchant Details Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all interactions with the Merchants like 
| add, edit & delete. 
|
*/
class MerchantDetailServiceProvider
{
	/**
     * Return view with all details regarding Merchants.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMerchantDetails($request)
    {
        $merchants = DB::table('merchants')
            ->join('users', 'merchants.user_id', '=', 'users.id')
            ->join('merchant_type', 'merchants.merchant_type_id', '=', 'merchant_type.id')

            ->select(
                    'merchants.id',
                    'users.name',
                    'merchants.company_name',
                    'merchants.company_address',
                    'merchants.landline_no',
                    'merchants.merchant_type_id',
                    'merchants.user_id',
                    'merchant_type.merchant_type'
                )
            ->get();

        return view('merchantDetails', ['merchants'=>$merchants]);
    }

	/**
     * Update the Merchant details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function updateMerchantDetail($input)
    {
        $this->validator($input)->validate();

        $id = $input['id'];
        
        $updatedMerchant = Merchant::where('id', $id)
            ->update([
                'merchant_type_id' => $input['merchant_type_id'],
                'company_name' => $input['company_name'],
                'company_address' => $input['company_address'],
                'landline_no' => $input['landline_no'],
            ]);

        if ($updatedMerchant) {
            return back()->with('success','Company Details Updated Successfully !');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }
    
    /**
     * Delete the Merchant details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function deleteMerchantDetail($input)
    {
        $user_id = $input['user_id'];
        $deletedMerchant = User::where('id', $user_id)->delete();

        if ($deletedMerchant) {
            return response()->json('Merchant Details Deleted !');
        }
        return response()->json('Oops! Something Went Wrong.');
    }

  /**
   * Get a validator for update merchant request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
		'company_name' => 'required|string|max:255',
		'company_address' => 'required|string|max:255',
		'landline_no' => 'required|regex:/[0-9]{10}/|digits:10',
    ]);
  }

}
