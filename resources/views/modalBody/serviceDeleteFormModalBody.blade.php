@foreach ($item as $item)
	<div class="login-box-body row">
		<div class="col-xs-1"></div>

		<div class="col-xs-10">
			<form method="POST" action="{{ url('/myServices') }}">
				{{ csrf_field() }}
				<p>Are you sure you want to delete <strong>{{ $item->item }}</strong> from your added services ?</p>
				<p class="text-warning"><small>This action cannot be undone.</small></p>

				<div class="form-group">
					<input id="operation" type="hidden" class="form-control" name="operation" value="delete" required>
				</div>

				<div class="form-group">
					<input id="id" type="hidden" class="form-control" name="id" value="{{ $item->id }}" required>
				</div>

				<div class="form-group pull-right">
					<input type="submit" class="btn btn-danger" value="Delete">
				</div>

			</form>
		</div>

	</div>
@endforeach	      
