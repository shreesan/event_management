<div class="modal fade" id="{{ $options['id'] }}">
  <div class="modal-dialog {{ $options['cssClass'] }}">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" onclick="Modal.close();" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title text-center">
        	{!! $options['title'] !!}
        </h4>
      </div>
      <!-- The below format converts string into HTML, the string will not auto escape  -->
      <div class="modal-body">
        {!! $options['body'] !!}
      </div>

      <div class="modal-footer" >
        <div id="footerBtnArea">
          <button type="button" onclick="Modal.close();" class="btn btn-default pull-right" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>
</div>