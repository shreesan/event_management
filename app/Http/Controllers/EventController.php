<?php

namespace App\Http\Controllers;

use App\Event;
use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\User;
use View;

use App\Services\StripePaymentProvider;
use App\Services\CartServiceProvider;
use App\Services\CelebrationsEventServiceProvider;
use App\Services\BillingServiceProvider;
use App\Services\GoogleServiceProvider;

class EventController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  |  Event Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling all operations related to 
  | events - create, edit, delete, view. Along with options for add to cart,
  | generate bill, add event to google calendar using API and download DomPDF bill.  
  |
  */

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Function to check for Authentication of the Host.
   *
   * @return \Illuminate\Http\Response
   */
  private function checkAuth($request)
  {
      $host = config('constants.host');
      $user = Auth::user()->user_type_id;

      //Check for the Host.
      if ($user === $host) { 
        return 1;
      }
      else {
        abort(404,'Unauthorized Access !!!');
      }
  }

  /**
   * Add service items to Cart.
   *
   * @return \Illuminate\Http\Response
   */
  public function addToCart(Request $request)
  {
    if ($this->checkAuth($request) == 1) {
      $service = new CartServiceProvider();
      $cart = $service->cartAdd($request);
      return $cart;
    }
  }

  /**
   * Show the Booked Events page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showMyEvents(Request $request)
  {
    if ($this->checkAuth($request) == 1) {
      $eventService = new CelebrationsEventServiceProvider();
      return $eventService->eventView($request);
    }
  }

  /**
   * Main function that handles all requests related to events - create, update and 
   * delete events, add events to Google calendar and generate event Bills.
   *
   * @return \Illuminate\Http\Response
   */
  public function eventDetail(Request $request)
  {
      $input = $request->all();
      
      $cartService = new CartServiceProvider();
      $eventService = new CelebrationsEventServiceProvider();
      $billService = new BillingServiceProvider();
      $googleService = new GoogleServiceProvider();
      $payService = new StripePaymentProvider();

      switch ($input['operation']) {
        case 'add':
          return $eventService->eventAdd($input);
          break;

        case 'edit':
          return $eventService->eventUpdate($input);
          break;

        case 'serviceDelete':
          return $cartService->cartDelete($input);
          break;

        case 'bill':
          return $billService->generateBill($input);
          break;

        case 'download':
          return $billService->downloadPDF($input);
          break;

        case 'google':
          return $googleService->addToGoogleCalendar($input);
          break;

        case 'api':
          return $googleService->getGoogleClient($input);
          break;

        case 'payment':
          return $payService->payment($input);
          break;

        case 'delete':
          return $eventService->eventDelete($input);
          break;
      }
  }

}
