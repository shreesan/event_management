<?php

namespace App\Http\Middleware;

use Closure;

use DB;

class ApiAuth
{
    /**
     * Handle an incoming request - To Authenticate the Incoming API requests.
     *
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $method = $request->method();
        header('Content-type: application/json');

        if ($request->isMethod('post')) {
            if ($request->api_token) {
                $users = DB::table('users')->pluck('api_token')->toArray();

                if (!in_array($request->api_token, $users, true)) {
                    $data = ['Not Authenticated'];
                    return response()->json($data);
                }
            }
            else
            {
                $data = ['Invalid Request'];
                return response()->json($data);
            }
        }
        else {
            /*$data = ['Only POST Requests Allowed'];
            return response()->json($data);*/

            if( request()->wantsJson() )
            {
                $data = ['Only POST Requests Allowed'];
                return response()->json($data);
            }
            else 
            {
                return response()->view('apiReturnPage');
            }

        }

        return $next($request);
    }
}
