<!DOCTYPE html>
<html>
<head>
	<title>API Documentation</title>
</head>
<body>
	<div style="border: 2px solid black;padding: 3%; margin-left: 15%;margin-right: 15%;">
		<h1>API Documentation for Celebrations...</h1>
		<p>To use the APIs of Celebrations, one needs to be authenticated.</p>
		
		<h3>Authentication of the User</h3>
		
		<p>Once a new user is registered, a unique API Token is generated by the system, which is stored in the Database. <br> The user can see his/her API Token, once succcessfully logged in, in the top right dropdown navbar.</p>
		
		<h3>The process of using API :</h3>
		
		<p>Using Postman, we can test the API as:</p>

		<ul>
			<li>One needs to hit the URL : 
				<strong><u>
					http://172.16.9.196/event_management/public/api/apiGetData
				</u></strong> 
			</li>
			<br>
			<li>For the <strong>Events</strong>, send the data as form-data :
				<ul>
					<li>
						<strong>api_token</strong> : 5d53e67e270aa (host) or 5d53e6a7cedaf (merchant)
					</li>
					<li>
						<strong>request_type</strong> : event
					</li>
				</ul>
			</li>
			<br>
			<li>For the <strong>Venues</strong>, send the data as form-data :
				<ul>
					<li>
						<strong>api_token</strong> : 5d53e67e270aa (host) or 5d53e6a7cedaf (merchant)
					</li>
					<li>
						<strong>request_type</strong> : venue
					</li>
				</ul>
			</li>
		</ul>
	</div>
</body>
</html>