<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use Validator;

use App\Event;
use App\BookedService;

use Debugbar; //To use Debugbar
use DB;

/*
|--------------------------------------------------------------------------
|  Event Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all interactions with the Events -View, Add, Update & 
| Delete, along with Validations. 
|
*/
class CelebrationsEventServiceProvider
{
	/**
     * Function to View all the events.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventView($request)
    {
        $id = Auth::user()->id;

        $today = date('Y-m-d');

        // The following code marks the events whose date have passed, as "Completed".
        DB::table('events')
            ->where('user_id', $id)
            ->where('date', '<', $today)
            ->update(['is_complete' => 1]);

        $carts = DB::table('booked_services')
            ->join('events', 'booked_services.events_booking_id', '=', 'events.booking_id')
            ->join('service_items', 'booked_services.service_id', '=', 'service_items.id')
            ->join('merchants', 'service_items.merchant_id', '=', 'merchants.id')
            ->join('users', 'merchants.user_id', '=', 'users.id')
            ->join('merchant_type', 'merchants.merchant_type_id', '=', 'merchant_type.id')

            ->where('events.user_id', '=', $id)
            
            ->select(
                        'booked_services.events_booking_id',
                        'booked_services.status',
                        'booked_services.id',
                        'service_items.item',
                        'service_items.cost',
                        'service_items.description',
                        'merchants.company_name',
                        'merchants.landline_no',
                        'merchants.merchant_type_id',
                        'merchant_type.merchant_type',
                        'users.contact_no',
                        'users.email'
                    )
            ->orderBy('cost', 'desc')
            ->get();

        // $eventsBooked = $carts->pluck('events_booking_id')->unique();

        $events = DB::table('events')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')
            ->join('payment_mode', 'events.payment_mode_id', '=', 'payment_mode.id')

            // ->whereIn('events.booking_id', $eventsBooked)
            ->where('events.user_id', '=', $id)

            ->select(
                        'events.id',
                        'events.booking_id',
                        'events.bill_status',
                        'events.event_type_id',
                        'events.payment_mode_id',
                        'users.name', 
                        'event_type.event_type',
                        'payment_mode.payment_mode',
                        'events.date',
                        'events.payment_status',
                        'events.google_status',
                        'events.bill_amount',
                        'events.no_of_guests',
                        'events.is_complete'
                    )

            ->orderBy('date', 'desc')
            ->get()
            ->unique();

        return view('events/myBookedEvents', ['events'=>$events], ['carts'=>$carts]);
    }
    /**
     * Function to add a new event.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventAdd($input)
    {
        $this->validator($input)->validate();

        $unique_id = uniqid();
        $user = Auth::user()->id;

        $eventAdded = Event::create([
            'booking_id' => $unique_id,
            'user_id' => $user,
            'event_type_id' => $input['event_type_id'],
            'payment_mode_id' => $input['payment_mode_id'],
            'date' => $input['date'],
            'no_of_guests' => $input['no_of_guests'],
            'is_complete' => '0',
            'bill_status' => '0',
            'bill_amount' => '0',
            'google_status' => '0',
            'payment_status' => '0',
        ]);

        if ($eventAdded) {
            return back()->with('success','Event added successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    
    }

    /**
     * Function to Update an Event.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventUpdate($input)
    {
        $this->validator($input)->validate();

        $id = $input['booking_id'];

        Event::where('booking_id', $id)
            ->update([
                'event_type_id' => $input['event_type_id'],
                'payment_mode_id' => $input['payment_mode_id'],
                'no_of_guests' => $input['no_of_guests'],
                'date' => $input['date'],
                'bill_status' => '0',
                'bill_amount' => '0',
            ]);

        $eventUpdated = BookedService::where('events_booking_id', $id)
            ->update([
                'status' => '0', // Any update in event details will make the status of the all the services in cart as "Waiting". 
            ]);

        if ($eventUpdated) {
            return back()->with('success','Event Updated Successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Function to delete an Event.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function eventDelete($input)
    {
        $id = $input['booking_id'];
        $eventDeleted = Event::where('booking_id', $id)->delete();
        
        if ($eventDeleted) {
            return back()->with('success','Event Deleted successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Get a validator for an incoming add event request.
     *
     * @param  array  $input
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $input)
    {
      return Validator::make($input, [
        'no_of_guests' => 'required|numeric|min:50|max:1500',
        'date' => 'required|date|after:today',
      ]);
    }
}