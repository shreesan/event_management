@section('flash_content')

	<script>
		$(document).ready(function(){
			$("#myAlert").fadeOut(4000);
		});
	</script>

	@if ($message = Session::get('success'))
	<!-- <div class="alert alert-success alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
	  <strong>{{ $message }}</strong>
	</div> -->

	<!-- <div class="alert alert-success alert-dismissible">
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <h4><i class="icon fa fa-check"></i> Alert!</h4>
	    {{ $message }}.
	</div> -->

		<div id="myAlert" class="alert alert-success alert-dismissible">
			<h4>
				<i class="icon fa fa-check"></i>
				Alert!
			</h4>
			{{ $message }}
		</div>
	
	@endif


	@if ($message = Session::get('error'))
		<div id="myAlert" class="alert alert-danger alert-dismissible">
			<h4>
				<i class="icon fa fa-ban"></i>
				Alert!
			</h4>
			{{ $message }}
		</div>
	@endif


	@if ($message = Session::get('warning'))
		<div id="myAlert" class="alert alert-warning alert-dismissible">
			<h4>
				<i class="icon fa fa-warning"></i>
				Alert!
			</h4>
			{{ $message }}
		</div>
	@endif


	@if ($message = Session::get('info'))
		<div id="myAlert" class="alert alert-info alert-dismissible">
			<h4>
				<i class="icon fa fa-info"></i>
				Alert!
			</h4>
			{{ $message }}
		</div>
	@endif


	@if ($errors->any())
	<div id="myAlert" class="alert alert-danger">
		<h4>
			<i class="icon fa fa-ban"></i>
			Alert!
		</h4>
		Please check the form for errors.
	</div>
	@endif

@endsection
