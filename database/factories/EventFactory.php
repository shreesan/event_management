<?php

use Faker\Generator as Faker;

$factory->define(App\Event::class, function (Faker $faker) {
    return [
        'booking_id' => str_random(13),
        'event_type_id' => mt_rand(1, 6),
        'payment_mode_id' => mt_rand(1, 3),
        'date' => $faker->date,
        'no_of_guests' => mt_rand(50, 1000),
        'is_complete' => '0',
        'bill_status' => '0',
        'bill_amount' => '0',
        'google_status' => '0',
        'payment_status' => '0',
    ];
});
