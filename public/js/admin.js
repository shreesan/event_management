var admin = {
  deleteUser: function(el){
    el    = $(el);
    id    = el.data('id');
    name  = el.data('name');
    title = 'Delete User Data';
    msg   = 'Are you sure to delete the records of "<strong>'+name+'</strong>" from the Database ?<br ><p class="text-warning"><small>This action cannot be undone.</small></p>';

    params = {};
    params.operation = 'delete';
    params.id = id;

    Modal.confirmBox(title, msg, 'Delete', function(){
      Modal.loaderOpen();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/home',
        type: 'POST',
        data: params,
        success: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        },
        error: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        }
      });
    });
  },

  deleteMerchant: function(el){
    el    = $(el);
    id    = el.data('id');
    company_name  = el.data('company_name');
    user_id  = el.data('user_id');
    title = 'Delete Merchant Data';
    msg   = 'Are you sure to delete the records of "<strong>'+company_name+'</strong>" from the Database ?<br ><p class="text-warning"><small>This action cannot be undone.</small></p>';

    params = {};
    params.operation = 'delete';
    params.id = id;
    params.user_id = user_id;

    Modal.confirmBox(title, msg, 'Delete', function(){
      Modal.loaderOpen();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/merchantDetails',
        type: 'POST',
        data: params,
        success: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        },
        error: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        }
      });
    });
  }
};