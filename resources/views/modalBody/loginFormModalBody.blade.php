<form method="POST" action="{{ route('login') }}">
	{{ csrf_field() }}
		<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required autofocus>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
      <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
    </div>

	  <div>
	  	<a class="btn btn-primary form-group pull-left" href="javascript:void(0);" onclick="openRegisterModal('drawRegisterForm', 'HomeController', 'Register a new membership')">Register a new membership
      </a>
      <button type="submit" class="btn btn-primary form-group pull-right">Login</button>
	  </div>


</form>
