@extends('layouts.mylayout')

@section('navigation')
  
@endsection

@section('sidebar')
  @parent
    <li class="active">
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>
  
@endsection

@section('first_content')
  <div style="font-weight: bold; color: black;">
    <h2>Welcome to our APIs Page
      <small> Celebrations.com</small>
    </h2>
  </div>
@endsection

@section('content')
	<div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Venues API</h3>
        <p>Get the list of Venues easily using our API</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/halls/hall1.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="#" class="small-box-footer">Find all the venues here
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  
	<div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Events API</h3>
        <p>Get the list of events easily using our API</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/cakes/cake.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="#" class="small-box-footer">Find all the events here
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

@endsection
