@foreach ($cart as $cart)
  <div class="login-box-body row">
    <div class="col-xs-1"></div>

    <div class="col-xs-10">
    	<form method="POST" action="{{ url('/pendingRequests') }}">
  		{{ csrf_field() }}
        
        <p>Are you sure you want to Reject <strong>{{ $cart->item }}</strong> for <strong>{{ $cart->event_type }}</strong> on <strong>{{ date('jS \of F Y , l', strtotime($cart->date)) }}</strong> ?</p>
        <p class="text-warning"><small>This action cannot be undone.</small></p>

        <div class="form-group">
          <input id="operation" type="hidden" class="form-control" name="operation" value="reject" required>
        </div>

        <div class="form-group">
          <input id="id" type="hidden" class="form-control" name="id" value="{{ $cart->id }}" required>
        </div>

        <div class="form-group pull-right">
        	<input type="submit" class="btn btn-danger" value="Reject">
        </div>

      </form>
    </div>

  </div>
@endforeach