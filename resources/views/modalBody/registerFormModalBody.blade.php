<form method="POST" action="{{ route('register') }}">
	{{ csrf_field() }}
		<div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
      <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Full name" required autofocus>
      <span class="glyphicon glyphicon-user form-control-feedback"></span>

      @if ($errors->has('name'))
        <span class="help-block">
          <strong>{{ $errors->first('name') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group">
      <div class="radio-inline">
        <label>
          <input type="radio" name="user_type_id" id="host" value="2" required/> Host
        </label>
      </div>

      <div class="radio-inline">
        <label>
          <input type="radio" name="user_type_id" id="merchant" value="3"/> Merchant
        </label>
      </div>
      
      <div id="merchantOnly" class="myDiv">
        <div class="form-group">
          <label for="merchant_type_id">Select Merchant Type: </label>

          <select class="form-control" name="merchant_type_id" id="merchant_type_id">
            <option value="1" required>Caterer</option>
            <option value="2">Hall Owner</option>
            <option value="3">Decorator</option>
            <option value="4">Florist</option>
            <option value="5">Pandit</option>
            <option value="6">Baker</option>
          </select>

        </div>

        <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }} has-feedback">
          <input id="company_name" type="text" class="form-control" name="company_name" value="{{ old('company_name') }}" placeholder="Company Name" required autofocus>
          <span class="glyphicon glyphicon-globe form-control-feedback"></span>

          @if ($errors->has('company_name'))
            <span class="help-block">
              <strong>{{ $errors->first('company_name') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }} has-feedback">
          <textarea id="company_address" type="text" class="form-control" name="company_address" rows="2" placeholder="Company Address" required>{{ old('company_address') }}</textarea>
          <span class="glyphicon glyphicon-pushpin form-control-feedback"></span>

          @if ($errors->has('company_address'))
            <span class="help-block">
              <strong>{{ $errors->first('company_address') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('landline_no') ? ' has-error' : '' }} has-feedback">
          <input id="landline_no" type="text" class="form-control" name="landline_no" value="{{ old('landline_no') }}" placeholder="Landline Number" required>
          <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>

          @if ($errors->has('landline_no'))
            <span class="help-block">
              <strong>{{ $errors->first('landline_no') }}</strong>
            </span>
          @endif
        </div>
      </div>
    </div>

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
      <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email" required>
      <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

      @if ($errors->has('email'))
        <span class="help-block">
          <strong>{{ $errors->first('email') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} has-feedback">
      <textarea id="address" type="text" class="form-control" name="address" rows="2" placeholder="Personal Address" required>{{ old('address') }}</textarea>
      <span class="glyphicon glyphicon-home form-control-feedback"></span>

      @if ($errors->has('address'))
        <span class="help-block">
          <strong>{{ $errors->first('address') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }} has-feedback">
      <input id="contact_no" type="text" class="form-control" name="contact_no" value="{{ old('contact_no') }}" placeholder="Contact Number" required>
      <span class="glyphicon glyphicon-phone form-control-feedback"></span>

      @if ($errors->has('contact_no'))
        <span class="help-block">
          <strong>{{ $errors->first('contact_no') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback">
      <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>
      <span class="glyphicon glyphicon-lock form-control-feedback"></span>

      @if ($errors->has('password'))
        <span class="help-block">
          <strong>{{ $errors->first('password') }}</strong>
        </span>
      @endif
    </div>

    <div class="form-group has-feedback">
      <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Retype password" required>
      <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
    </div>

	  <div>
	  	<a class="btn btn-primary form-group pull-left" href="javascript:void(0);" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">I already have a membership
      </a>
      <button type="submit" class="btn btn-primary form-group pull-right">Register</button>
	  </div>

</form>

<script type="text/javascript">
	$('input[type="radio"]').click(function(){
    var demovalue = $(this).val();
    $("#merchantOnly").hide();

    if (demovalue == 2) {
      $('#merchantOnly').find('input, textarea, button, select').attr('disabled','disabled');
    }
    else {
      $('#merchantOnly').find('input, textarea, button, select').attr('disabled', false);
      $("#merchantOnly").show();
    }
  });
</script>