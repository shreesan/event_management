INDIAN EVENT MANAGEMENT SYSTEM
------------------------------------------------------------------------------------------------------------------------------------------------
INTRODUCTION
------------------------------------------------------------------------------------------------------------------------------------------------
This web application is a platform that shall help arrange and manage services needed for a typical Indian Event. Similar to an online marketplace, the hosts can choose from event services provided by merchants, as per their needs. The system helps remove the mess created by maintaining excel sheets, notes, long lists, etc, and helps both event hosts and merchants to review their event bookings.


PROJECT DESCRIPTION
------------------------------------------------------------------------------------------------------------------------------------------------
India being a country of many celebrations and events starting from small birthday parties and 21 st day rituals, to anniversary parties and house warming and to once in a life time grand celebrations like marriages, Thread ceremonies, etc.

For the invitees, these functions are just as good as to visit, have good food and enjoy their selves, but on the contrary, the hosts spend days under constant tension and work hard to make ends meet, just for making the event successful. Whether its a get-together or a wedding in the family, its difficult to manage hundreds of tasks, handling caterers, guests, hotels, gifts, etc ! In addition, diaries, excel sheets, memos, notes, long endless lists, etc just mess up and become unmanageable for the host.

In such a situation, the proposed event management system can really come handy, and loosen the tight work schedules. The system being capable of
handling many fields with an easy to use UI/UX, can really help the hosts to meet up their expectations of hosting a flawless event, as well as steal out precious time to enjoy with their loved ones.

It also works for the benefit of merchants for dealing with their products, ie the merchants like a flower decorator can register and hence can get bookings and review all his orders in a single place.
------------------------------------------------------------------------------------------------------------------------------------------------


