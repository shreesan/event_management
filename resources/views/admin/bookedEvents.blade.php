@extends('layouts.mylayout')

@section('navigation')
	
@endsection

@section('sidebar')
  @parent
    <li>
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>
    
    <li class="header">
      <i class="fa fa-folder-open"></i> RESOURCES
    </li>

    <li>
      <a href="{{ url('merchantDetails') }}">
        <i class="fa fa-user"></i> 
        <span> View Merchant Details</span>
      </a>
    </li>

    <li class="active">
      <a href="{{ url('bookedEvents') }}">
        <i class="fa fa-pencil-square-o"></i> 
        <span> View Booked Events</span>
      </a>
    </li>

@endsection

@section('content')
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>Manage <b>Events</b></h2>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Booking ID</th>
          <th>Booked By</th>
          <th>Event Type</th>
          <th>Date Selected</th>
          <th>No. of Guests</th>
          <th>Payment Mode</th>
          <th>Status</th>
          <th>Services Selected</th>
          <th>Actions</th>
          <th>Invoice</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($events as $event)
          <tr>
            <td>{{ $event->id }}</td>
            <td>{{ $event->booking_id }}</td>
            <td>{{ $event->name }}</td>
            <td>{{ $event->event_type }}</td>
            <td>{{ date('jS \of F Y , l', strtotime($event->date)) }}</td>
            <td>{{ $event->no_of_guests }}</td>
            <td>{{ $event->payment_mode }}</td>

            @if (($event->is_complete) == 0)
              <td>Upcoming</td>
            @else
              <td>Completed</td>
            @endif

            <td>
              <!-- Show services in cart, if the event is not complete. -->
              @if (($event->is_complete) == 0)
                <a href="#modal-cart-{{ $event->booking_id }}" class="edit" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                  <i class="material-icons" data-toggle="tooltip" title="View Cart">shopping_cart</i>
                </a>

                <!-- Services Selected in Cart Modal -->
                <div class="modal fade modal-cart-{{ $event->booking_id }}" id="modal-cart-{{ $event->booking_id }}" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center">Services selected for 
                          <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}, booked by <strong>{{ $event->name }}</strong></h4>
                      </div>
                      <div class="modal-body">
                        <div class="login-box-body row">
                            <div class="table-wrapper">
                              <div class="table-title">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <h2><b>Services</b></h2>
                                  </div>
                                </div>
                              </div>
                              <table class="table table-striped table-hover">
                                <thead>
                                  <tr>
                                    <th>Service</th>
                                    <th>Cost</th>
                                    <th>Description</th>
                                    <th>Provided by</th>
                                    <th>Contact Details</th>
                                    <th>Status</th>
                                    <th>Delete</th>
                                  </tr>
                                </thead>

                                <tbody>
                                @foreach ($carts as $cart)
                                  <tr>
                                    @if ( $event->booking_id == $cart->events_booking_id )
                                      <td>{{ $cart->item }}</td>
                                      <td>&#8377 {{ $cart->cost }}</td>
                                      <td>{{ $cart->description }}</td>
                                      <td>{{ $cart->company_name }}</td>

                                      <td>{{ $cart->landline_no }}, {{ $cart->contact_no }}, {{ $cart->email }}</td>
                                      
                                      <td>
                                        @if (($cart->status) == 0)
                                          <h4>
                                            <span class="label label-warning">Waiting</span>
                                          </h4>
                                        @elseif (($cart->status) == 1)
                                          <h4>
                                            <span class="label label-success">Accepted</span>
                                          </h4>
                                        @else
                                          <h4>
                                            <span class="label label-danger">Rejected</span>
                                          </h4>
                                        @endif
                                      </td>

                                      <td>
                                        <!-- Delete only unbilled services. -->
                                        @if (($event->bill_status) == 0)
                                          <a href="#modal-delete-{{ $cart->id }}" class="delete" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                                            <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                          </a>

                                          <!-- Delete Service Modal -->
                                          <div id="modal-delete-{{ $cart->id }}" class="modal fade">
                                            <form method="POST" action="{{ url('/bookedEvents') }}">
                                              {{ csrf_field() }}
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">                      
                                                    <h4 class="modal-title">Delete Service</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div class="col-xs-1"></div>

                                                    <div class="col-xs-10">
                                                      <p>Are you sure you want to delete added service <strong>{{ $cart->item }}</strong>, booked by <strong>{{ $event->name }}</strong> for <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                                                      <p class="text-warning"><small>This action cannot be undone.</small></p>

                                                      <div class="form-group">
                                                        <input id="operation" type="hidden" class="form-control" name="operation" value="serviceDelete" required>
                                                      </div>

                                                      <div class="form-group">
                                                        <input id="id" type="hidden" class="form-control" name="id" value="{{ $cart->id }}" required>
                                                      </div>
                                                      </div>                    
                                                  </div>
                                                  <div class="modal-footer">
                                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                                    <input type="submit" class="btn btn-danger" value="Delete">
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                        @else
                                          <i class="material-icons" data-toggle="tooltip" title="Cannot Delete Billed Service">&#xE872;</i>
                                        @endif

                                      </td>

                                    @endif
                                  </tr>
                                @endforeach
                                </tbody>
                              </table>
                            </div>

                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              @else
                <i class="material-icons" data-toggle="tooltip" title="No Changes Needed">shopping_cart</i>
              @endif
            </td>
            
            <td>
              <!-- Allow edit and delete services only when the event is not complete and not billed. -->
              @if ((($event->is_complete) == 0) && (($event->bill_status) == 0))
                <a href="#modal-edit-{{ $event->id }}" class="edit" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                  <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
                </a>

                <!-- Edit Event Modal -->
                <div class="modal fade" id="modal-edit-{{ $event->id }}">
                  <form method="POST" action="{{ url('/bookedEvents') }}">
                    {{ csrf_field() }}
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          <h4 class="modal-title text-center">Edit Event Data</h4>
                        </div>
                        <div class="modal-body">
                          <div class="login-box-body row">
                            <div class="col-xs-1"></div>

                            <div class="col-xs-10">
                              <div class="form-group">
                                <input id="operation" type="hidden" class="form-control" name="operation" value="editEvent" required>
                              </div>

                              <div class="form-group">
                                <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                              </div>

                              <div class="form-group">
                                <label for="event_type_id">Select Event Type: </label>
                                <select class="form-control" name="event_type_id" id="event_type_id">
                                  
                                  <option value="1" required {{ old("event_type_id", $event->event_type_id) == 1 ? 'selected' : '' }}>Marriage</option>

                                  <option value="2" {{ old("event_type_id", $event->event_type_id) == 2 ? 'selected' : '' }}>Birthday</option>

                                  <option value="3" {{ old("event_type_id", $event->event_type_id) == 3 ? 'selected' : '' }}>Anniversary</option>

                                  <option value="4" {{ old("event_type_id", $event->event_type_id) == 4 ? 'selected' : '' }}>Grihapravesh</option>

                                  <option value="5" {{ old("event_type_id", $event->event_type_id) == 5 ? 'selected' : '' }}>Thread Ceremony</option>
                                  
                                  <option value="6" {{ old("event_type_id", $event->event_type_id) == 6 ? 'selected' : '' }}>Annaprasanna</option>

                                </select>
                              </div>

                              <div class="form-group">
                                <label for="payment_mode_id">Select Payment Mode: </label>
                                <select class="form-control" name="payment_mode_id" id="payment_mode_id">
                                  
                                  <option value="1" required {{ old("payment_mode_id", $event->payment_mode_id) == 1 ? 'selected' : '' }}>Cash</option>
                                  <option value="2" {{ old("payment_mode_id", $event->payment_mode_id) == 2 ? 'selected' : '' }}>Cheque</option>
                                  <option value="3" {{ old("payment_mode_id", $event->payment_mode_id) == 3 ? 'selected' : '' }}>Online Transfer</option>

                                </select>
                              </div>

                              <div class="form-group{{ $errors->has('no_of_guests') ? ' has-error' : '' }} has-feedback">
                                <label for="no_of_guests">Number Of Guests: </label>
                                <input id="no_of_guests" type="number" class="form-control" name="no_of_guests" value="{{ $event->no_of_guests }}" placeholder="No. of Guests" min="50" required>
                                <span class="glyphicon glyphicon-user form-control-feedback">
                                </span>

                                @if ($errors->has('no_of_guests'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('no_of_guests') }}</strong>
                                  </span>
                                @endif
                              </div>

                              <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} has-feedback">
                                <label for="date">Pick A Date: </label>
                                <input id="date" type="date" class="form-control" name="date" value="{{ date('Y-m-d', strtotime($event->date)) }}" min="{{ date('Y-m-d', strtotime($event->date)) }}" required>
                                <span class="glyphicon glyphicon-calendar form-control-feedback">
                                </span>

                                @if ($errors->has('date'))
                                  <span class="help-block">
                                    <strong>{{ $errors->first('date') }}</strong>
                                  </span>
                                @endif
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="modal-footer">
                          <button type="submit" class="btn btn-primary form-group">Update</button>
                        </div>

                      </div>
                    </div>
                  </form>  
                </div>

                <a href="#modal-delete-{{ $event->id }}" class="delete" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                  <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                </a>

                <!-- Delete Event Modal -->
                <div id="modal-delete-{{ $event->id }}" class="modal fade">
                  <form method="POST" action="{{ url('/bookedEvents') }}">
                    {{ csrf_field() }}
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">                      
                          <h4 class="modal-title">Delete Event Data</h4>
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        </div>
                        <div class="modal-body">
                          <div class="col-xs-1"></div>

                          <div class="col-xs-10">
                            <p>Are you sure you want to delete <strong>{{ $event->event_type }}</strong> booked by <strong>{{ $event->name }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                            <p class="text-warning"><small>This action cannot be undone.</small></p>

                            <div class="form-group">
                              <input id="operation" type="hidden" class="form-control" name="operation" value="delete" required>
                            </div>

                            <div class="form-group">
                              <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                            </div>
                          </div>                    
                        </div>
                        <div class="modal-footer">
                          <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                          <input type="submit" class="btn btn-danger" value="Delete">
                        </div>
                      </div>
                    </div>
                  </form>
                </div>

              @else
                <i class="material-icons" data-toggle="tooltip" title="No Changes Needed">&#xE254;</i>
               
                <i class="material-icons" data-toggle="tooltip" title="No Changes Needed">&#xE872;</i>
              @endif

            </td>

            <td>
              <!-- Allow bill download and addition of the event into Google Calendar only when the event is confirmed and bill is generated. -->
              @if (($event->bill_status) == 1)
                <a href="#modal-show-bill-{{ $event->booking_id }}" class="edit" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                  <i class="material-icons" data-toggle="tooltip" title="Get Invoice">file_download</i>
                </a>
                <div class="modal fade modal-show-bill-{{ $event->booking_id }}" id="modal-show-bill-{{ $event->booking_id }}" tabindex="-1" role="dialog">
                  <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center">Bill Generated for 
                          <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}</h4>
                      </div>
                      <div class="modal-body">
                        <div class="login-box-body row">
                          <h4>Event booked by : <b>{{ $event->name }}</b></h4>
                          <h4>No. of Guests : <b>{{ $event->no_of_guests }}</b></h4>
                          <h4>Payment Mode : <b>{{ $event->payment_mode }}</b></h4>
                            <div class="table-wrapper">
                              <div class="table-title">
                                <div class="row">
                                  <div class="col-sm-6">
                                    <h2>Bill No. <b>{{ $event->booking_id }}</b></h2>
                                  </div>
                                </div>
                              </div>
                              <table class="table table-striped table-hover">
                                <thead>
                                  <tr>
                                    <th>Service</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Provided by</th>
                                    <th>Contact</th>
                                    <th>Amount</th>
                                  </tr>
                                </thead>

                                <tbody>
                                  @foreach ($carts as $cart)
                                    <tr>
                                      <!-- Only show Accepted services in bill table. -->
                                      @if ( $event->booking_id == $cart->events_booking_id && ($cart->status) == 1 )
                                        <td>{{ $cart->item }}</td>
                                        <td>{{ $cart->description }}</td>
                                        <td>{{ $cart->merchant_type }}</td>
                                        <td>{{ $cart->company_name }}</td>
                                        <td>{{ $cart->landline_no }}, {{ $cart->contact_no }}</td>

                                        <td>&#8377
                                          @if ($cart->merchant_type_id == 1)
                                            {{ $cart->cost*$event->no_of_guests }}
                                          @else
                                            {{ $cart->cost }}
                                          @endif
                                        </td>
                                       
                                      @endif
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>

                              <div class="table-title">
                                <div class="row">
                                  <div class="col-sm-8"></div>
                                  <div class="col-sm-4">
                                    <h4>Total Amount : &#8377 <b>{{ $event->bill_amount }}</b></h4>
                                  </div>
                                </div>
                              </div>

                            </div>

                        </div>
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal" aria-label="Close">Close
                        </button>
                      </div>

                    </div>
                  </div>
                </div>
              @else
                <i class="material-icons" data-toggle="tooltip" title="Bill Not Generated">file_download</i>
              @endif
            </td>
            
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
