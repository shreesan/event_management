<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Merchant;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use Auth; //to use the Auth facade in the controller

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'address' => 'required|string|max:255',
            'contact_no' => 'required|regex:/[0-9]{10}/|digits:10|unique:users',
            'password' => 'required|string|min:6|confirmed',

            'company_name' => 'string|max:255',
            'company_address' => 'string|max:255',
            'landline_no' => 'regex:/[0-9]{10}/|digits:10|unique:merchants',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        /*$api_token = uniqid();

        return User::create([
            'name' => $data['name'],
            'user_type_id' => $data['user_type_id'],
            'email' => $data['email'],
            'address' => $data['address'],
            'contact_no' => $data['contact_no'],
            'password' => bcrypt($data['password']),
            'google_auth_code' => '0',
            'api_token' => $api_token,
        ]);*/

        $api_token = uniqid();

        $user = User::create([
            'name' => $data['name'],
            'user_type_id' => $data['user_type_id'],
            'email' => $data['email'],
            'address' => $data['address'],
            'contact_no' => $data['contact_no'],
            'password' => bcrypt($data['password']),
            'google_auth_code' => '0',
            'api_token' => $api_token,
        ]);
        
        if ($data['user_type_id'] == '3') {
            Merchant::create([
                'user_id' => $user['id'],
                'merchant_type_id' => $data['merchant_type_id'],
                'company_name' => $data['company_name'],
                'company_address' => $data['company_address'],
                'landline_no' => $data['landline_no'],
            ]);
        }

        return $user;
    }

}

/*$string_to_encrypt="shreesan";

$password="shreesanAdmin";

$encrypted_string=openssl_encrypt($string_to_encrypt,"AES-128-ECB",$password);

$decrypted_string=openssl_decrypt($encrypted_string,"AES-128-ECB",$password);

echo $decrypted_string;*/