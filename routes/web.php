<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Web Routes are defined here for the project, which are loaded by the
| RouteServiceProvider within a group which contains the "web" middleware group. 
|
*/

Route::get('/', function () {
    return view('celebrations');
});

/*Route::get('/apiPage', function () {
    return view('apiHomePage');
});*/

/*Route::group(['middleware' => 'apiauth'],function()
{
	Route::post('testApi', 'Apis\ApiAuthController@testApi')->name('testApi');
	Route::any('apiGetData', 'Apis\ApiAuthController@apiGetData')->name('apiGetData');
});*/

Route::group(['middleware' => 'disablepreventback'],function()
{
	Auth::routes();

	Route::get('/home', 'HomeController@index')->name('home');

	// Routes for the Events as viewed by the Host :

	Route::get('/myEvents', 'EventController@showMyEvents')->name('/myEvents');

	// Routes for the Services as viewed by the Merchant :

	// Route::get('/merchantDetails', 'MerchantController@showMerchantDetails')->name('/merchantDetails');

	Route::get('/myServices', 'MerchantController@services')->name('/myServices');

	Route::get('/pendingRequests', 'MerchantController@showPendingRequests')->name('/pendingRequests');

	Route::post('/pendingRequests', 'MerchantController@approveRequests');


	// Image Uploading , and service operations as done by the Merchant :

	Route::post('/myServices', 'MerchantController@serviceDetails');


	// Routes for the resources as viewed by the Admin :

	Route::get('/merchantDetails', 'AdminController@showMerchant')->name('/merchantDetails');

	Route::post('/merchantDetails', 'AdminController@merchantDetail');

	Route::get('/bookedEvents', 'AdminController@showEvents')->name('/bookedEvents');

	Route::post('/bookedEvents', 'AdminController@eventDetail');

	Route::post('/home', 'AdminController@userDetail');


	// Routes for the services to be chosen by hosts :

	Route::get('/halls', 'EventServiceController@showHalls')->name('/halls');

	Route::get('/decoration', 'EventServiceController@showDecoration')->name('/decoration');

	Route::get('/catering', 'EventServiceController@showCatering')->name('/catering');

	Route::get('/florist', 'EventServiceController@showFlorist')->name('/florist');

	Route::get('/pandit', 'EventServiceController@showPandit')->name('/pandit');

	Route::get('/baker', 'EventServiceController@showBaker')->name('/baker');


	// All Event Operations as done by the Host :

	Route::post('/myEvents', 'EventController@eventDetail');

	Route::post('/addToCart', 'EventController@addToCart');

	// Route::get('/bill', 'EventController@showBill')->name('/bill');

	Route::post('/ajax', 'AjaxController@processAjax');
	
	Route::any('/venueLocation', 'AdminController@setLocationOnMap');
		
});