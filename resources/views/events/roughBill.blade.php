<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Celebrations</title>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <link href="{{ asset('css/design.css') }}" rel="stylesheet">
  <link href="{{ asset('css/layout.css') }}" rel="stylesheet">
  <script src="{{ asset('js/script.js') }}"></script>

</head>

<body>
  <section class="content">
    <div class="table-wrapper">
      @foreach ($events as $event)
        <div class="table-title">
          <div class="row">
            <div class="col-sm-2">
              <header class="main-header">
                <div class="logo">
                  <img src="dist/img/sss.jpg" class="img-fluid" id="myLogo">
                  <span id="logoTitle"> Celebrations</span>
                </div>
              </header>
            </div>
            <div class="col-sm-10 text-center">
              <h2 class="modal-title ">Invoice for <strong>{{ $event->eventType->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}</h2>
            </div>
          </div>
        </div>

        <table class="table table-striped table-hover">
          <thead>
            <h4>
              Booking ID : <b>{{ $event->booking_id }}</b> &emsp; 
              Event booked by : <b>{{ $event->getEventUser->name }}</b> &emsp; 
              No. of Guests : <b>{{ $event->no_of_guests }}</b> &emsp; 
              Payment Mode : <b>{{ $event->payType->payment_mode }}</b>
            </h4>

            <tr>
              <th>Service</th>
              <th>Description</th>
              <th>Provided by</th>
              <th>Contact</th>
              <th>Amount</th>
            </tr>
          </thead>

          <tbody>
              @foreach ($carts as $cart)
                <tr>
                  @if ( $event->booking_id == $cart->events_booking_id && ($cart->status) == 1 )
                    <td>{{ $cart->getServiceItem->item }}</td>
                    <td>{{ $cart->getServiceItem->description }}</td>
                    <td>{{ $cart->getServiceItem->getMerchant->company_name }}<br>( {{ $cart->getServiceItem->getMerchant->merchantType->merchant_type }} )</td>
                    <td>{{ $cart->getServiceItem->getMerchant->landline_no }}, {{ $cart->getServiceItem->getMerchant->getMerchantUser->contact_no }} , {{ $cart->getServiceItem->getMerchant->getMerchantUser->email }}</td>

                    <td> &#8377
                      @if (($cart->getServiceItem->getMerchant->merchantType->id) == 1)
                        {{ $cart->getServiceItem->cost*$event->no_of_guests }}
                      @else
                        {{ $cart->getServiceItem->cost }}
                      @endif
                      /-
                    </td>
                   
                  @endif
                </tr>
              @endforeach
          </tbody>
        </table>
        <div class="table-title">
          <div class="row">
            <div class="col-sm-9"></div>
            <div class="col-sm-3" align="right">
              <h4>Total Amount : &#8377 <b>{{ $event->bill_amount }} /-</b></h4>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </section>
</body>