@section('card_content')
<!-- The Services Cards -->
  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Event Venue</h3>
        <p>Book banquet halls and lawns ideal for your event...</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/halls/hall1.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/halls') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red"> 
      <div class="inner">
        <h3>Decorations</h3>
        <p>The best decorations to turn your event into a dreamland !</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/mandaps/mandap5.jpeg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/decoration') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Catering Services</h3>
        <p>Taste of good food lasts forever ! Have a fine dine...</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/items/paneer1.jpeg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/catering') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Flowers and Garlands</h3>
        <p>Fresh Flowers - The scent of pure love....</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/malas/mala22.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/florist') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Pujas and Rituals</h3>
        <p>Get the best of Brahmans and Pandits for your special occasion...</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/gates/gate8.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/pandit') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

  <div class="col-lg-3 col-xs-6" style="width: 50%">
    <div class="small-box bg-red">
      <div class="inner">
        <h3>Luxurious Cakes</h3>
        <p>A fresh cake for welcoming a fresh new life...</p>
      </div>
      <div class="icon">
        <img src="dist/img/images/cakes/cake.jpg" class="img-fluid" style="height: 80px; border-radius: 50%">
      </div>
      <a href="{{ url('/baker') }}" class="small-box-footer">View All Options 
        <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>

@endsection