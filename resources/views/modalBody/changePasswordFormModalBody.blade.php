<form method="POST" action="{{ url('/home') }}">
	{{ csrf_field() }}
		<div class="form-group">
	    <input id="operation" type="hidden" class="form-control" name="operation" value="changePassword" required>
	  </div>

	  <div class="form-group{{ $errors->has('current_password') ? ' has-error' : '' }} has-feedback">
	    <label for="new_password">Enter your current Password :</label>

	    <input id="current_password" type="password" class="form-control" name="current_password" placeholder="Current Password" required>
	    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

	    @if ($errors->has('current_password'))
	      <span class="help-block">
	        <strong>{{ $errors->first('current_password') }}</strong>
	      </span>
	    @endif
	  </div>

	  <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }} has-feedback">
	    <label for="new_password">Enter New Password :</label>

	    <input id="new_password" type="password" class="form-control" name="new_password" placeholder="New Password" required>
	    <span class="glyphicon glyphicon-lock form-control-feedback"></span>

	    @if ($errors->has('new_password'))
	      <span class="help-block">
	        <strong>{{ $errors->first('new_password') }}</strong>
	      </span>
	    @endif
	  </div>

	  <div class="form-group has-feedback">
	    <label for="new_password_confirm">Re-Enter New Password</label>
	    <input id="new_password-confirm" type="password" class="form-control" name="new_password_confirmation" placeholder="New Password" required>
	    <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
	  </div>

	  <div class="form-group pull-right">
	  	<button type="submit" class="btn btn-primary form-group">Update</button>
	  </div>

</form>