<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Celebrations</title>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="{{ asset('css/design.css') }}" rel="stylesheet">
  <link href="{{ asset('css/layout.css') }}" rel="stylesheet">

  <script src=" https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

  <script src="{{ asset('js/script.js') }}"></script>
  <script src="{{ asset('js/modal.js') }}"></script>
  <script src="{{ asset('js/user.js') }}"></script>

  <script type="text/javascript">
    var AJAX_URL = "{{ Config::get('constants.AJAX_URL') }}";
  </script>

  @if (count($errors) > 0)
    <script>
      $( document ).ready(function() {
        $('#modal-login').modal('show');
        // $('#modal-register').modal('show');
      });
    </script>
  @endif

</head>
<body class="hold-transition skin-red-light sidebar-mini" id="myBody">
<div>
  <header class="main-header">
    <a href="#" class="logo">
      <img src="dist/img/sss.jpg" class="img-fluid" id="myLogo">
      <span id="logoTitle"> Celebrations</span>
    </a>
    <nav class="navbar navbar-static-top">

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          @if (Route::has('login'))
            @auth
              <li>
                <a href="{{ url('/home') }}">
                <span class="hidden-xs">Home</span></a>
              </li>
            @else
              <li>
                <a href="{{ url('/apiPage') }}">
                  Our APIs
                </a>
              </li>

              <li>
                <a href="#" data-toggle="modal" data-target="#modal-about" data-backdrop="static" data-keyboard="false">About us</a>
              </li>

              <li>
                <a href="javascript:void(0);" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">Login
                </a>
              </li>

              <li>
                <a href="javascript:void(0);" onclick="openRegisterModal('drawRegisterForm', 'HomeController', 'Register a new membership')">Register
                </a>
              </li>


            @endauth
          @endif
        </ul>
      </div>
    </nav>
  </header>

  <div>
    <section class="content-header">
      <div>
        <h2>Welcome to Celebrations.com
          <small>We manage, you enjoy !</small>
        </h2>
      </div>
    </section>

    <section class="content">
      <!-- carousal -->
      <div class="col-md-4" id="myCarousal">
        <div class="box box-solid" id="carousalBox">
          <div class="box-header with-border">
            <h3 class="box-title">Some highlights of our services...</h3>
          </div>
          <div class="box-body">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="2000">
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="3" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="4" class=""></li>
                <li data-target="#carousel-example-generic" data-slide-to="5" class=""></li>
              </ol>
              <div class="carousel-inner">
                <div class="item active">
                  <img src="dist/img/khai.jpg" alt="First slide" id="caraousalImage" >
                </div>
                <div class="item">
                  <img src="dist/img/celebrations.png" alt="Second slide" id="caraousalImage">
                </div>
                <div class="item">
                  <img src="dist/img/gulab_jamun.jpg" alt="Third slide" id="caraousalImage">
                </div>
                <div class="item">
                  <img src="dist/img/grihapravesh.jpg" alt="Fourth slide" id="caraousalImage">
                </div>
                <div class="item">
                  <img src="dist/img/thali.jpg" alt="Fifth slide" id="caraousalImage">
                </div>
                <div class="item">
                  <img src="dist/img/anniversary.jpeg" alt="Sixth slide" id="caraousalImage">
                </div>
              </div>
              <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                <span class="fa fa-angle-left"></span>
              </a>
              <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                <span class="fa fa-angle-right"></span>
              </a>
            </div>
          </div>
        </div>
      </div>

      <!-- The Event Cards -->
      <div class="col-lg-3 col-xs-6" id="myCards">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Vivah</h3>
            <p>The Great Indian Weddings !</p>
          </div>
          <div class="icon">
            <img src="dist/img/vivah.jpg" class="img-fluid" id="cardImage">
          </div>
          <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
            Book services now
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" id="myCards">
        <div class="small-box bg-red"> 
          <div class="inner">
            <h3>Grihapravesh</h3>
            <p>First steps into a new home.</p>
          </div>
          <div class="icon">
            <img src="dist/img/griha.jpg" class="img-fluid" id="cardImage">
          </div>
          <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
            Book services now
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" id="myCards">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>Brata Ghar</h3>
              <p>The Sacred Thread Ceremony</p>
            </div>
            <div class="icon">
              <img src="dist/img/paita.jpg" class="img-fluid" id="cardImage">
            </div>
            <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
              Book services now
              <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
      </div>

      <div class="col-lg-3 col-xs-6" id="myCards">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Annaprasana</h3>
            <p>Celebrating a child's first whole meal</p>
          </div>
          <div class="icon">
            <img src="dist/img/anna.jpg" class="img-fluid" id="cardImage">
          </div>
          <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
            Book services now
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" id="myCards">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Birthday Party</h3>
            <p>A day to celebrate !</p>
          </div>
          <div class="icon">
            <img src="dist/img/bday.jpg" class="img-fluid" id="cardImage">
          </div>
          <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
            Book services now
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>

      <div class="col-lg-3 col-xs-6" id="myCards">
        <div class="small-box bg-red">
          <div class="inner">
            <h3>Anniversary</h3>
            <p>Lovely memories to cherish.</p>
          </div>
          <div class="icon">
            <img src="dist/img/ani.jpg" class="img-fluid" id="cardImage">
          </div>
          <a href="javascript:void(0);" class="small-box-footer" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">
            Book services now
            <i class="fa fa-arrow-circle-right"></i>
          </a>
        </div>
      </div>

      <!-- About us modal, only required in this page -->
      <div class="modal fade" id="modal-about">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h2 class="modal-title text-center">Who we are...</h2>
            </div>
            <div class="modal-body">
              <div class="login-box-body row">
                <div class="col-xs-1"></div>
                <div class="col-xs-10">
                  <img src="dist/img/myTeam.jpg" alt="team" id="aboutImage" >
                  <h4 class="text-center">Celebrations was established in 2015 to provide a focused approach towards the event planning. With years of experience in the event industry, we stand strong with the most creative, enthusiastic and committed team members, who have developed their expertise.</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </section>

  </div>

  <footer class="text-center" id="myFooter">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright © 2019-2020 <a href="#">Shreesan Studio</a>.</strong> All rights reserved.
  </footer>
</div>

<!-- Space for importing modals -->
  <div id="siteModalDiv"></div>

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>

</body>

</html>