<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToBookedServices extends Migration
{
    /**
     * Run the migrations.
     *
     * Added new column for status of the service booked.
     * @return void
     */
    public function up()
    {
        Schema::table('booked_services', function (Blueprint $table) {
            $table->integer('status')->unsigned();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('booked_services', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
