<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceItem extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Service Item Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "service_items" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item', 'cost', 'merchant_id', 'description', 'image',
    ];

    /**
     * Get the Merchant of the ServiceItem.
     * The Foreign key names are overridden.
     */
    public function getMerchant()
    {
        return $this->belongsTo('App\Merchant', 'merchant_id');
    }

    /**
     * Get the BookedServices of the ServiceItem.
     * The Foreign key names are overridden.
     */
    public function getBookedServices()
    {
        return $this->hasMany('App\BookedService', 'service_id');
    }
}