<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
|  AJAX Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for serving all AJAX requests. 
|
*/
class AjaxServiceProvider
{
   /**
	* Function to handle all the AJAX requests for Modals.
	*
	* @return \Illuminate\Http\Response
	*/
    public function processAjaxModal(Request $request)
    {
		/*$data= $request->id;
		// dd($data);

		// return response()->json(',your ID is '.$data);
		return response()->json([
        	view('modal')->with('data',$data)->render()
     	]);*/

    	/*$data = $request->id;


     	$className 		= (isset($request->className) && $request->className)?$request->className:'';
		$functionName 	= (isset($request->functionName) && $request->functionName)?$request->functionName:'';

		if(!$className || !$functionName){
			error_log("ERROR: Missing Parameters");
			return false;
		}*/

		return response()->json([
        	view('modal')->with('data',$data)->render()
     	]);
        
    }
	
}
