<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Event Type Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "event_type" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_type';

    /**
     * Get the Events for the EventType.
     * The Foreign key names are overridden.
     */
    public function events()
    {
        return $this->hasMany('App\Event', 'event_type_id');
    }
}
