<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Merchant extends Model
{
    use Notifiable;

    /*
    |--------------------------------------------------------------------------
    |  Merchant Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "merchant" table and its relationships. 
    |
    */

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','company_name', 'merchant_type_id', 'company_address', 'landline_no',
    ];

    /**
     * Get the MerchantType of the Merchant.
     * The Foreign key names are overridden.
     */
    public function merchantType()
    {
        return $this->belongsTo('App\MerchantType', 'merchant_type_id');
    }

    /**
     * Get the User of the Merchant.
     * The Foreign key names are overridden.
     */
    public function getMerchantUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the ServiceItems of the Merchant.
     * The Foreign key names are overridden.
     */
    public function hasServiceItems()
    {
        return $this->hasMany('App\ServiceItem', 'merchant_id');
    }
}
