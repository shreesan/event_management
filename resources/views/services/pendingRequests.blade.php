@extends('layouts.mylayout')

@section('navigation')
  
@endsection

@section('sidebar')
  @parent
  <li>
    <a href="{{ url('/home') }}">
      <i class="fa fa-home"></i> Home
    </a>
  </li>
  
  <li class="header">
    <i class="fa fa-folder-open"></i> RESOURCES
  </li>

  <li>
    <a href="{{ url('/myServices') }}">
      <i class="fa fa-check"></i> 
        <span> View My Services</span>
    </a>
  </li>

  <li class="active">
    <a href="{{ url('/pendingRequests') }}">
      <i class="fa fa-download"></i>
        <span class="hidden-xs"> Pending Requests</span>
    </a>
  </li>

@endsection

@section('first_content')
  
@endsection

@section('content')
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>Pending <b>Requests</b></h2>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Service Requested</th>
          <th>Event</th>
          <th>Date</th>
          <th>Payment mode</th>
          <th>No. of Guests</th>
          <th>Booked by</th>
          <th>Phone No.</th>
          <th>email</th>
          <th>Status</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($carts as $cart)
          <tr>
            <!-- Show the booked services to respective merchant. -->
            <td>{{ $cart->item }}</td>
            <td>{{ $cart->event_type }}</td>
            <td>{{ date('jS \of F Y , l', strtotime($cart->date)) }}</td>
            <td>{{ $cart->payment_mode }}</td>
            <td>{{ $cart->no_of_guests }}</td>
            <td>{{ $cart->name }}</td>
            <td>{{ $cart->contact_no }}</td>
            <td>{{ $cart->email }}</td>
            
            <td>
              <!-- Accept or Reject a requested service, when it is waiting (status = 0 ) -->
              @if (($cart->status) == 0)
                <div class="dropdown">
                  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">Action
                    <span class="fa fa-caret-down"></span>
                  </button>
                     
                  <ul class="dropdown-menu">
                    <li>
                      <a href="javascript:void(0);" onclick="openAcceptRequestModal('drawAcceptRequestForm', 'MerchantController', '{{ $cart->id }}', 'Accept the Request')">Accept
                      </a>
                    </li>

                    <li>
                      <a href="javascript:void(0);" onclick="openRejectRequestModal('drawRejectRequestForm', 'MerchantController', '{{ $cart->id }}', 'Reject the Request')">Reject
                      </a>
                    </li>

                  </ul>
                </div>
              @elseif (($cart->status) == 1)
                <h4>
                  <span class="label label-success">Accepted</span>
                </h4>
                
              @else
                <h4>
                  <span class="label label-danger">Rejected</span>
                </h4>
              @endif

            </td>
          </tr>

        @endforeach
      </tbody>
    </table>
  </div>



@endsection
