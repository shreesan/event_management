<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /*
    |--------------------------------------------------------------------------
    | User Item Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "users" table and its relationships. 
    |
    */


    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_type_id', 'email', 'address', 'contact_no', 'password', 'google_auth_code','api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the UserType of the User.
     * The Foreign key names are overridden.
     */
    public function userType()
    {
        return $this->belongsTo('App\UserType', 'user_type_id');
    }

    /**
     * Get the Events for the User.
     * The Foreign key names are overridden.
     */
    public function hasEvents()
    {
        return $this->hasMany('App\Event', 'user_id');
    }

    /**
     * Get the Merchants for the User.
     * The Foreign key names are overridden.
     */
    public function hasMerchants()
    {
        return $this->hasMany('App\Merchant', 'user_id');
    }
}
