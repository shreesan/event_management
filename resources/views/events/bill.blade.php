<!--
|--------------------------------------------------------------------------
|  Bill format to be downloaded as PDF.
|--------------------------------------------------------------------------
|
| This HTML file contains inline CSS, so as to be able to get rendered when   
| the bill is to be downloaded.  
|
-->
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <style>
      table.myTable, th.myTable, td.myTable {
        border: 1px solid black;
        border-collapse: collapse;
      }
      th.myTable, td.myTable {
        padding: 10px;
      }
      th.myTable {
        text-align: left;
      }
      .myText {
        font-size: 120%;
      }
    </style>
  </head>

  <body>
  @foreach ($events as $event)
    <table style="width: 100%;">
      <tr>
        <td style="width: 20%; text-align: center;">
          <img src="dist/img/celebrations_logo.png" style="height: 50px; border-radius: 10%">
        </td>

        <td>
          <h2 style="width: 80%; text-align: center;">
            <u>
              Invoice for <strong>{{ $event->eventType->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}
            </u>
          </h2>
        </td>
        
      </tr>
    </table>

    <table style="width:100%">
      <tr style="text-align: center;">
        <td style="width: 25%;">
          <p class="myText">
            Booking ID : <b>{{ $event->booking_id }}</b>
          </p>
        </td>
        <td style="width: 25%;">
          <p class="myText">
            Event booked by : <b>{{ $event->getEventUser->name }}</b>
          </p>
        </td>
        <td style="width: 25%;">
          <p class="myText">
            No. of Guests : <b>{{ $event->no_of_guests }}</b>
          </p>
        </td>
        <td style="width: 25%;">
          <p class="myText">
            Payment Mode : <b>{{ $event->payType->payment_mode }}</b>
          </p>
        </td>
      </tr>
    </table>

    <table class="myTable" style="width:100%">
      <thead>
        <tr>
          <th class="myTable" style="width: 10%;">Service</th>
          <th class="myTable" style="width: 30%;">Description</th>
          <th class="myTable" style="width: 20%;">Provided by</th>
          <th class="myTable" style="width: 20%;">Contact</th>
          <th class="myTable" style="width: 10%;">Amount</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($myCart as $cart)
          <tr>
            <td class="myTable">{{ $cart->getServiceItem->item }}</td>
            <td class="myTable">{{ $cart->getServiceItem->description }}</td>
            <td class="myTable">{{ $cart->getServiceItem->getMerchant->company_name }} ( {{ $cart->getServiceItem->getMerchant->merchantType->merchant_type }} )</td>
            <td class="myTable">{{ $cart->getServiceItem->getMerchant->landline_no }}, {{ $cart->getServiceItem->getMerchant->getMerchantUser->contact_no }} , {{ $cart->getServiceItem->getMerchant->getMerchantUser->email }}</td>

            <td class="myTable"> Rs.
              @if (($cart->getServiceItem->getMerchant->merchantType->id) == 1)
                {{ $cart->getServiceItem->cost*$event->no_of_guests }}
              @else
                {{ $cart->getServiceItem->cost }}
              @endif
              /-
            </td>
          </tr>
        @endforeach
      </tbody>

      <tfoot>
        <tr>
          <td colspan="4" style="text-align: right;">
            <p class="myText">
              <b>Total Amount : Rs. {{ $event->bill_amount }} /-</b>
            </p>
          </td>
          <td></td>
        </tr>
      </tfoot>
      
    </table>
  @endforeach
  </body>
</html>