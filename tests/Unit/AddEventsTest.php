<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Event;

class AddEventsTest extends TestCase
{
	use WithFaker;

    /**
     * Test cases for Adding an Event.
     *
     * @return void
     */
    public function testAddEvents()
    {
        $event = factory(Event::class)->create([
            'user_id' => '4',
        ]);
        $response = $this->post('/myEvents', [
            '_token' => csrf_token(),
        ]);
        $response->assertRedirect('/login');

        /*$response = $this->call('POST', '/myEvents', array(
        	'_token' => csrf_token(),
        	'booking_id' => str_random(13),
        	'event_type_id' => '1',
        	'payment_mode_id' => '1',
        	'no_of_guests' => '64',
        	'date' => $this->faker->date,
	    ));

	    $this->assertEquals(302, $response->getStatusCode());*/
    }
}
