<form id="locationData" target="iframe" method="post" action="{{ url('/venueLocation') }}" >
	{{ csrf_field() }}
    <input type="hidden" name="location" value="{{$location}}"/>
    <input type="hidden" name="venueName" value="{{$venueName}}"/>
    <input type="submit"/>
</form>

<iframe name="iframe" src="{{ url('/venueLocation') }}" width="100%" height="400" style="border:  1px solid black;"></iframe>

<script>
	$(document).ready(function(){
		var locationForm = document.getElementById("locationData");
		locationForm.style.display = "none";
		locationForm.submit();
	});
</script>
