<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use Hash; //Current password provided by user should match the password stored in the database. We check this by using Hash::check method.
use DB;
use Validator;

use App\User;


/*
|--------------------------------------------------------------------------
|  User Detail Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all interactions with users as done by Admin. 
|
*/
class UserDetailServiceProvider
{
	/**
     * Change the User Password.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function changePassword($input)
    {
    	$this->validator($input)->validate();

        $current_password = $input['current_password'];
        $new_password = $input['new_password'];

        if (!(Hash::check($current_password, Auth::user()->password))) {
            return back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($current_password, $new_password) == 0){
            return back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $id = Auth::user()->id;

        $passwordChanged = User::where('id', $id)
            ->update([
                'password' => bcrypt($new_password),
            ]);

        if ($passwordChanged) {
            return back()->with('success','Password Changed Successfully !');
        }
        return back()->with('error','Oops! Something Went Wrong.');        
    }


    /**
     * Update the User details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function updateUserDetail($input)
    {
    	$this->validator($input)->validate();

        $id = $input['id'];
        
        $userUpdated = User::where('id', $id)
            ->update([
                'name' => $input['name'],
                'address' => $input['address'],
                'email' => $input['email'],
                'contact_no' => $input['contact_no'],
                'google_auth_code' => '0',
            ]);

        if ($userUpdated) {
            return back()->with('success','User Details Updated Successfully !');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Delete the User details.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function deleteUserDetail($input)
    {
        $id = $input['id'];
        $userDeleted = User::where('id', $id)->delete();
        
        if ($userDeleted) {
            return response()->json('User Details Deleted !');
        }
        return response()->json('Oops! Something Went Wrong.');
    }

    /**
     * Get a validator for an incoming update user request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // Validation for user details update
        if ($data['operation'] == 'editUser') {
            return Validator::make($data, [
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255',
                'address' => 'required|string|max:255',
                'contact_no' => 'required|regex:/[0-9]{10}/|digits:10',
            ]);
        }

        // Validation for user change password
        elseif ($data['operation'] == 'changePassword') {
            return Validator::make($data, [
                'current_password' => 'required',
                'new_password' => 'required|string|min:6|confirmed',
            ]);
        }

        
    }
}
