@extends('layouts.mylayout')

@section('navigation')
  
@endsection

@section('sidebar')
  @parent
    <li>
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>

    <li class="header">
      <i class="fa fa-gift"></i> SERVICES
    </li>

    <li>
      <a href="{{ url('/halls') }}">
        <i class="fa fa-bank"></i> Halls & Venues
      </a>
    </li>
    
    <li>
      <a href="{{ url('/decoration') }}">
        <i class="fa fa-snowflake-o"></i> Decorations
      </a>
    </li>
    
    <li>
      <a href="{{ url('/catering') }}">
        <i class="fa fa-cutlery"></i> Catering
      </a>
    </li>
    
    <li>
      <a href="{{ url('/florist') }}">
        <i class="fa fa-leaf"></i> Flowers and Garlands
      </a>
    </li>
    
    <li>
      <a href="{{ url('/pandit') }}">
        <i class="fa fa-bell"></i> Pujas and Rituals
      </a>
    </li>
    
    <li>
      <a href="{{ url('/baker') }}">
        <i class="fa fa-birthday-cake"></i> Bakery
      </a>
    </li>

    <li class="header">
      <i class="fa fa-folder-open"></i> RESOURCES
    </li>

    <li class="active">
      <a href="{{ url('/myEvents') }}">
        <i class="fa fa-pencil-square-o"></i> 
        <span> My Events </span>
      </a>
    </li>
  
@endsection

@section('first_content')
  
@endsection

@section('content')
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>My <b>Events</b></h2>
        </div>
        <div class="col-sm-6">
          <!-- Add Event Option -->
          <a href="#" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modal-event" data-backdrop="static" data-keyboard="false">  <i class="fa fa-gift"></i> 
            <span><strong>Add Event</strong></span>
          </a>
        </div>
      </div>
    </div>

    <table id="eventsTable" class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Booking ID</th>
          <th>Event Type</th>
          <th>Date Selected</th>
          <th>No. of Guests</th>
          <th>Payment Mode</th>
          <th>Status</th>
          <th>Payment Status</th>
          <th>Actions</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($events as $event)
          <tr>
            <td>{{ $event->booking_id }}</td>
            <td>{{ $event->event_type }}</td>
            <td>{{ date('jS \of F Y , l', strtotime($event->date)) }}</td>
            <td>{{ $event->no_of_guests }}</td>
            <td>{{ $event->payment_mode }}</td>

            @if (($event->is_complete) == 0)
              <td>Upcoming</td>
            @else
              <td>Completed</td>
            @endif

            @if (($event->payment_status) == 0)
              <td>Unpaid</td>
            @else
              <td>Paid</td>
            @endif

            <!-- <td>
              Carts was here

            </td> -->

            <td>

              <div class="dropdown">
                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">Action
                  <span class="fa fa-caret-down"></span>
                </button>

                <ul class="dropdown-menu">
                  <li>
                    <a href="#modal-cart-{{ $event->booking_id }}" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                      View Cart
                    </a>
                  </li>

                  <!-- Edit and Delete Events allowed only when the event is upcoming and bill not generated. -->
                  @if ((($event->is_complete) == 0) && (($event->bill_status) == 0))
                    <li>
                      <a href="#modal-edit-{{ $event->booking_id }}" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                        Edit
                      </a>
                    </li>

                    <li>
                      <a href="#modal-delete-{{ $event->booking_id }}" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                        Delete
                      </a>
                    </li>
                  @endif

                  <!-- Show bill modal when the bill is generated i.e. the event is confirmed. -->
                  @if (($event->bill_status) == 1)
                    <li>
                      <a href="#modal-show-bill-{{ $event->booking_id }}" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                        Invoice
                      </a>
                    </li>
                  @endif
                  
                </ul>
              </div>

              <!-- My services in cart Modal -->
              <div id="modal-cart-{{ $event->booking_id }}" class="modal fade">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title text-center">Services selected for 
                        <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}</h4>
                    </div>
                    <div class="modal-body">
                      <div class="login-box-body row">
                          <div class="table-wrapper">
                            <div class="table-title">
                              <div class="row">
                                <div class="col-sm-6">
                                  <h2>My <b>Services</b></h2>
                                </div>
                              </div>
                            </div>

                            <!-- Used a count variable to check whether the cart  is empty or not 
                              & acceptedCount variable to check whether cart has atleast one accepted service to be billed -->
                            @php
                              $count = 0;
                              $acceptedCount = 0;
                            @endphp

                            @foreach ($carts as $cart)
                              @if ($event->booking_id == $cart->events_booking_id)
                                @php
                                  $count = $count + 1;
                                @endphp

                                @if ($cart->status == 1)
                                  @php
                                    $acceptedCount = $acceptedCount + 1;
                                  @endphp
                                @endif
                              @endif
                            @endforeach

                            @if ($count != 0)
                              <table class="table table-striped table-hover">
                                <thead>
                                  <tr>
                                    <th>Service</th>
                                    <th>Cost</th>
                                    <th>Description</th>
                                    <th>Provided by</th>
                                    <th>Contact</th>
                                    <th>Status</th>
                                    <th>Delete</th>
                                  </tr>
                                </thead>

                                <tbody>
                                  @foreach ($carts as $cart)
                                    <tr>
                                      @if ( $event->booking_id == $cart->events_booking_id )
                                        <td>{{ $cart->item }}</td>
                                        <td>&#8377 {{ $cart->cost }}</td>
                                        <td>{{ $cart->description }}</td>
                                        <td>{{ $cart->company_name }}</td>

                                        <td>{{ $cart->landline_no }}, {{ $cart->contact_no }}</td>
                                        
                                        <td>
                                          @if (($cart->status) == 0)
                                            <h4>
                                              <span class="label label-warning">Waiting</span>
                                            </h4>
                                          @elseif (($cart->status) == 1)
                                            <h4>
                                              <span class="label label-success">Accepted</span>
                                            </h4>
                                          @else
                                            <h4>
                                              <span class="label label-danger">Rejected</span>
                                            </h4>
                                          @endif
                                        </td>

                                        <td>
                                          <!-- Delete booked services only when the event is not completed, nor it's bill generated. -->
                                          @if ((($event->is_complete) == 0) && (($event->bill_status) == 0))
                                          <a href="#modal-delete-{{ $cart->id }}" class="delete" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                                            <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                          </a>

                                          <!-- Delete Service Modal HTML -->
                                          <div id="modal-delete-{{ $cart->id }}" class="modal fade">
                                            <form method="POST" action="{{ url('/myEvents') }}">
                                              {{ csrf_field() }}
                                              <div class="modal-dialog">
                                                <div class="modal-content">
                                                  <div class="modal-header">                      
                                                    <h4 class="modal-title">Delete Service</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                  </div>
                                                  <div class="modal-body">
                                                    <div class="col-xs-1"></div>

                                                    <div class="col-xs-10">
                                                      <p>Are you sure you want to delete added service <strong>{{ $cart->item }}</strong>, booked for your <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                                                      <p class="text-warning"><small>This action cannot be undone.</small></p>

                                                      <div class="form-group">
                                                        <input id="operation" type="hidden" class="form-control" name="operation" value="serviceDelete" required>
                                                      </div>

                                                      <div class="form-group">
                                                        <input id="id" type="hidden" class="form-control" name="id" value="{{ $cart->id }}" required>
                                                      </div>
                                                      </div>                    
                                                  </div>
                                                  <div class="modal-footer">
                                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                                    <input type="submit" class="btn btn-danger" value="Delete">
                                                  </div>
                                                </div>
                                              </div>
                                            </form>
                                          </div>
                                          @else
                                            <i class="material-icons" data-toggle="tooltip" title="Cannot Delete the services">&#xE872;</i>
                                          @endif
                                        </td>
                                      @endif
                                    </tr>
                                  @endforeach
                                </tbody>
                              </table>

                            @elseif ($count == 0 && $event->is_complete == 1)
                              <h3 class="text-center">Cannot add services to completed events.</h3>
                            @else
                              <!-- Provided link to add services, when none exist in cart. -->
                              <h3 class="text-center">
                                No Services in Cart. Click
                                <a href="{{ url('/home') }}">
                                  <u><span> here</span></u>
                                </a>
                                for services.
                              </h3>
                            @endif
                          </div>

                      </div>
                    </div>

                    <!-- Various checks for bill generation : -->
                    <div class="modal-footer">
                      <!-- No bill when cart is empty. -->
                      @if ($count == 0)
                        <button type="button" class="btn btn-primary" disabled="disabled" data-toggle="tooltip" title="No Services to Generate Bill.">Generate Bill</button>
                      @else
                        <!-- The only case where bill will be generated is when the event is not completed, no bill has been generated earlier, and the cart consists of atleast one "Accepted" service. -->
                        @if ((($event->bill_status) == 0) && (($event->is_complete) == 0) && ($acceptedCount != 0))
                          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-bill-{{ $event->booking_id }}" data-backdrop="static" data-keyboard="false">Generate Bill</button>

                        <!-- No bill when the bill is already generated. -->
                        @elseif (($event->bill_status) == 1)
                          <button type="button" class="btn btn-primary" disabled="disabled" data-toggle="tooltip" title="Bill Already Generated.">Generate Bill</button>

                        <!-- No bill when there is no accepted  services in cart. -->
                        @elseif ($acceptedCount == 0)
                          <button type="button" class="btn btn-primary" disabled="disabled" data-toggle="tooltip" title="No Services Accepted. Bill Cannot be generated.">Generate Bill</button>

                        <!-- No bill to be generated, if the event date has passed. -->
                        @else
                          <button type="button" class="btn btn-primary" disabled="disabled" data-toggle="tooltip" title="Event date has passed. Bill Cannot be generated.">Generate Bill</button>
                        @endif
                      @endif

                    </div>
                      
                    <!-- Generate Bill Modal HTML -->
                    <div id="modal-bill-{{ $event->booking_id }}" class="modal fade">
                      <form method="POST" action="{{ url('/myEvents') }}">
                        {{ csrf_field() }}
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">                      
                              <h4 class="modal-title">Generate Bill</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                              <div class="col-xs-1"></div>

                              <div class="col-xs-10">
                                <p>Are you sure to generate final bill for your event <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                                <p class="text-warning"><small>This action cannot be undone. The Event shall be confirmed with the accepted services.</small></p>

                                <div class="form-group">
                                  <input id="operation" type="hidden" class="form-control" name="operation" value="bill" required>
                                </div>

                                <div class="form-group">
                                  <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                                </div>

                              </div>                    
                            </div>
                            <div class="modal-footer">
                              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                              <input type="submit" class="btn btn-success" value="Get Bill">
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>
              </div>

              <!-- Edit Event Modal -->
              <div id="modal-edit-{{ $event->booking_id }}" class="modal fade">
                <form method="POST" action="{{ url('/myEvents') }}">
                  {{ csrf_field() }}
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center">Edit Your Event Details</h4>
                      </div>
                      <div class="modal-body">
                        <div class="login-box-body row">
                          <div class="col-xs-1"></div>

                          <div class="col-xs-10">
                            <div class="form-group">
                              <input id="operation" type="hidden" class="form-control" name="operation" value="edit" required>
                            </div>

                            <div class="form-group">
                              <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                            </div>

                            <div class="form-group">
                              <label for="event_type_id">Select Event Type: </label>
                              <select class="form-control" name="event_type_id" id="event_type_id">
                                <option value="1" required {{ old("event_type_id", $event->event_type_id) == 1 ? 'selected' : '' }}>Marriage</option>
                                <option value="2" {{ old("event_type_id", $event->event_type_id) == 2 ? 'selected' : '' }}>Birthday</option>
                                <option value="3" {{ old("event_type_id", $event->event_type_id) == 3 ? 'selected' : '' }}>Anniversary</option>
                                <option value="4" {{ old("event_type_id", $event->event_type_id) == 4 ? 'selected' : '' }}>Grihapravesh</option>
                                <option value="5" {{ old("event_type_id", $event->event_type_id) == 5 ? 'selected' : '' }}>Thread Ceremony</option>
                                <option value="6" {{ old("event_type_id", $event->event_type_id) == 6 ? 'selected' : '' }}>Annaprasanna</option>
                              </select>
                            </div>

                            <div class="form-group">
                              <label for="payment_mode_id">Select Payment Mode: </label>
                              <select class="form-control" name="payment_mode_id" id="payment_mode_id">
                                <option value="1" required {{ old("payment_mode_id", $event->payment_mode_id) == 1 ? 'selected' : '' }}>Cash</option>
                                <option value="2" {{ old("payment_mode_id", $event->payment_mode_id) == 2 ? 'selected' : '' }}>Cheque</option>
                                <option value="3" {{ old("payment_mode_id", $event->payment_mode_id) == 3 ? 'selected' : '' }}>Online Transfer</option>
                              </select>
                              
                            </div>

                            <div class="form-group{{ $errors->has('no_of_guests') ? ' has-error' : '' }} has-feedback">
                              <label for="no_of_guests">Number Of Guests: </label>
                              <input id="no_of_guests" type="number" class="form-control" name="no_of_guests" value="{{ $event->no_of_guests }}" placeholder="No. of Guests" min="50" required>
                              <span class="glyphicon glyphicon-user form-control-feedback">
                              </span>

                              @if ($errors->has('no_of_guests'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('no_of_guests') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} has-feedback">
                              <label for="date">Pick A Date: </label>
                              <input id="date" type="date" class="form-control" name="date" value="{{ date('Y-m-d', strtotime($event->date)) }}" min="{{ date('Y-m-d', strtotime($event->date)) }}" required>
                              <span class="glyphicon glyphicon-calendar form-control-feedback">
                              </span>

                              @if ($errors->has('date'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('date') }}</strong>
                                </span>
                              @endif
                            </div>
                          </div>

                          <div class="col-xs-1"></div>
                          
                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      <button type="submit" class="btn btn-primary form-group">Update</button>
                    </div>
                  </div>
                </form>
              </div>

              <!-- Delete Event Modal -->
              <div id="modal-delete-{{ $event->booking_id }}" class="modal fade">
                <form method="POST" action="{{ url('/myEvents') }}">
                  {{ csrf_field() }}
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">                      
                        <h4 class="modal-title">Delete Event</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      </div>
                      <div class="modal-body">
                        <div class="col-xs-1"></div>

                        <div class="col-xs-10">
                          <p>Are you sure you want to delete your added <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                          <p class="text-warning"><small>This action cannot be undone.</small></p>

                          <div class="form-group">
                            <input id="operation" type="hidden" class="form-control" name="operation" value="delete" required>
                          </div>

                          <div class="form-group">
                            <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                          </div>
                          </div>                    
                      </div>
                      <div class="modal-footer">
                        <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                        <input type="submit" class="btn btn-danger" value="Delete">
                      </div>
                    </div>
                  </div>
                </form>
              </div>

              <!-- Bill Modal -->
              <div id="modal-show-bill-{{ $event->booking_id }}" class="modal fade">
                <div class="modal-dialog modal-lg">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title text-center">Bill Generated for 
                        <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}</h4>
                    </div>
                    <div class="modal-body">
                      <div class="login-box-body row">
                        <h4>No. of Guests : <b>{{ $event->no_of_guests }}</b></h4>
                        <h4>Payment Mode : <b>{{ $event->payment_mode }}</b></h4>
                          <div class="table-wrapper">
                            <div class="table-title">
                              <div class="row">
                                <div class="col-sm-6">
                                  <h2>Bill No. <b>{{ $event->booking_id }}</b></h2>
                                </div>
                              </div>
                            </div>
                            <table class="table table-striped table-hover">
                              <thead>
                                <tr>
                                  <th>Service</th>
                                  <th>Description</th>
                                  <th>Category</th>
                                  <th>Provided by</th>
                                  <th>Contact</th>
                                  <th>Amount</th>
                                </tr>
                              </thead>

                              <tbody>
                                @foreach ($carts as $cart)
                                  <tr>
                                    @if ( $event->booking_id == $cart->events_booking_id && ($cart->status) == 1 )
                                      <td>{{ $cart->item }}</td>
                                      <td>{{ $cart->description }}</td>
                                      <td>{{ $cart->merchant_type }}</td>
                                      <td>{{ $cart->company_name }}</td>
                                      <td>{{ $cart->landline_no }}, {{ $cart->contact_no }}</td>

                                      <td> &#8377
                                        @if (($cart->merchant_type_id) == 1)
                                          {{ $cart->cost*$event->no_of_guests }}
                                        @else
                                          {{ $cart->cost }}
                                        @endif
                                        /-
                                      </td>
                                     
                                    @endif
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>

                            <div class="table-title">
                              <div class="row">
                                <div class="col-sm-8"></div>
                                <div class="col-sm-4" align="right">
                                  <h4>Total Amount : &#8377 <b>{{ $event->bill_amount }} /-</b></h4>
                                </div>
                              </div>
                            </div>

                          </div>

                      </div>
                    </div>

                    <!-- The various checks for adding event to the Google Calendar  -->
                    <div class="modal-footer">
                      <!-- Only case to add event to google calendar : When the event is not previously added to google and the event is not complete. -->
                      @if ((($event->google_status) == 0) && (($event->is_complete) == 0))
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-google-{{ $event->booking_id }}" data-backdrop="static" data-keyboard="false">Add Event to Google Calendar</button>

                      <!-- Cant add to Google Calendar : When the event is already completed. -->
                      @elseif ((($event->google_status) == 0) && (($event->is_complete) == 1))
                        <button type="button" class="btn btn-success" data-toggle="tooltip" title="Cannot Add Completed Events." disabled>Add Event to Google Calendar</button>

                      <!-- Cant add to Google Calendar : Already Added event. -->
                      @else
                        <button type="button" class="btn btn-success" data-toggle="tooltip" title="Event Already Added." disabled>Add Event to Google Calendar</button>

                      @endif

                      <!-- Download Bill option. -->
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-print-{{ $event->booking_id }}" data-backdrop="static" data-keyboard="false">Download Bill</button>

                      @if ($event->payment_mode_id == 3 && $event->payment_status == 0)
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-payment-{{ $event->booking_id }}" data-backdrop="static" data-keyboard="false">Pay Online</button>
                      @endif

                    </div>
                      
                    <!-- Add to Google Calendar Modal -->
                    <div id="modal-google-{{ $event->booking_id }}" class="modal fade">
                      <form method="POST" action="{{ url('/myEvents') }}">
                        {{ csrf_field() }}
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">                      
                              <h4 class="modal-title">Add Event to Google Calendar</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                              <div class="col-xs-1"></div>

                              <div class="col-xs-10">
                                <p>You sure to add your Event <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} to Google Calendar ?</p>
                                <p class="text-warning"><small>This action cannot be undone.</small></p>

                                <div class="form-group">
                                  <input id="operation" type="hidden" class="form-control" name="operation" value="google" required>
                                </div>

                                <div class="form-group">
                                  <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                                </div>

                                <div class="form-group">
                                  <input id="event_type_id" type="hidden" class="form-control" name="event_type_id" value="{{ $event->event_type_id }}" required>
                                </div>

                                <div class="form-group">
                                  <input id="date" type="hidden" class="form-control" name="date" value="{{ date('Y-m-d', strtotime($event->date)) }}" required>
                                </div>

                              </div>                    
                            </div>
                            <div class="modal-footer">
                              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                              <input type="submit" class="btn btn-success" value="Add">
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    <!-- Print Bill Modal -->
                    <div id="modal-print-{{ $event->booking_id }}" class="modal fade">
                      <form method="POST" action="{{ url('/myEvents') }}">
                        {{ csrf_field() }}
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">                      
                              <h4 class="modal-title">Download Bill</h4>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                              <div class="col-xs-1"></div>

                              <div class="col-xs-10">
                                <p>Are you sure to Download bill for your event <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }} ?</p>
                                <p class="text-warning"><small>Kindly avoid printing the bill on paper, until necessary. Save Paper. Save Trees.</small></p>

                                <div class="form-group">
                                  <input id="operation" type="hidden" class="form-control" name="operation" value="download" required>
                                </div>

                                <div class="form-group">
                                  <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                                </div>

                              </div>                    
                            </div>
                            <div class="modal-footer">
                              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                              <input type="submit" class="btn btn-success" value="Download">
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                    <!-- Pay online Modal -->
                    <div id="modal-payment-{{ $event->booking_id }}" class="modal fade">
                      <form method="POST" action="{{ url('/myEvents') }}">
                        {{ csrf_field() }}
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">                      
                              <h4 class="modal-title">
                                Pay for your Event <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}
                              </h4>
                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">
                              <p>4242424242424242 / 4012888888881881</p>
                              <div class="col-xs-1"></div>

                              <div class="col-xs-10">
                                
                                <div class="form-group">
                                  <input id="operation" type="hidden" class="form-control" name="operation" value="payment" required>
                                </div>

                                <div class="form-group">
                                  <input id="booking_id" type="hidden" class="form-control" name="booking_id" value="{{ $event->booking_id }}" required>
                                </div>

                                <div class="form-group">
                                  <input id="bill_amount" type="hidden" class="form-control" name="bill_amount" value="{{ $event->bill_amount }}" required>
                                </div>

                                <div class="form-group">
                                  <input id="description" type="hidden" class="form-control" name="description" value="{{ $event->event_type }} on {{ date('jS \of F Y , l', strtotime($event->date)) }}" required>
                                </div>

                                <div class="form-group">
                                  <label>Card Number</label>
                                  <input id="card_no" type="text" autocomplete='off' class='form-control card-number' size='20' name="card_no" placeholder="Enter Your Card Number" required autofocus>
                                </div>

                                <div class="row">
                                  <div class="col-xs-4">
                                    <div class="form-group">
                                      <label>Expiry Month</label>
                                      <select class="form-control" name="ccExpiryMonth" id="ccExpiryMonth">
                                        <option value='01' required>January</option>
                                        <option value='02'>February</option>
                                        <option value='03'>March</option>
                                        <option value='04'>April</option>
                                        <option value='05'>May</option>
                                        <option value='06'>June</option>
                                        <option value='07'>July</option>
                                        <option value='08'>August</option>
                                        <option value='09'>September</option>
                                        <option value='10'>October</option>
                                        <option value='11'>November</option>
                                        <option value='12'>December</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="col-xs-4">
                                    <div class="form-group">
                                      <label>Expiry Year</label>
                                      <input id="ccExpiryYear" type="text" autocomplete='off' class="form-control card-expiry-year" placeholder="YYYY" size="4" name="ccExpiryYear" title="Enter Valid Expiry year." required autofocus>
                                      
                                    </div>
                                  </div>

                                  <div class="col-xs-4">
                                    <div class="form-group">
                                      <label>CVV No.</label>
                                      <input id="cvvNumber" autocomplete="off" class="form-control card-cvc" placeholder="CVV" size="3" type="text" name="cvvNumber" title="Enter your Three digit CVV number." required autofocus>
                                      
                                    </div>
                                  </div>
                                </div>

                              </div>                    
                            </div>
                            <div class="modal-footer">
                              <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                              <input type="submit" class="btn btn-success" value="Pay">
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>

                  </div>
                </div>
              </div>

            </td>

          </tr>

        @endforeach
      </tbody>
    </table>
  </div>

  <!-- Add Event Modal -->
  <div class="modal fade" id="modal-event">
    <form method="POST" action="{{ url('/myEvents') }}">
      {{ csrf_field() }}
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title text-center">Add Your Event</h4>
          </div>
          <div class="modal-body">
            <div class="login-box-body row">
              <div class="col-xs-1"></div>

              <div class="col-xs-10">

                <div class="form-group">
                  <input id="operation" type="hidden" class="form-control" name="operation" value="add" required>
                </div>

                <div class="form-group">
                  <label for="event_type_id">Select Event Type: </label>
                  <select class="form-control" name="event_type_id" id="event_type_id">
                    <option value="1" required>Marriage</option>
                    <option value="2">Birthday</option>
                    <option value="3">Anniversary</option>
                    <option value="4">Grihapravesh</option>
                    <option value="5">Thread Ceremony</option>
                    <option value="6">Annaprasanna</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="payment_mode_id">Select Payment Mode: </label>
                  <select class="form-control" name="payment_mode_id" id="payment_mode_id">
                    <option value="1" required>Cash</option>
                    <option value="2">Cheque</option>
                    <option value="3">Online Transfer</option>
                  </select>
                </div>

                <div class="form-group{{ $errors->has('no_of_guests') ? ' has-error' : '' }} has-feedback">
                  <label for="no_of_guests">Number Of Guests: </label>
                  <input id="no_of_guests" type="number" class="form-control" name="no_of_guests" value="{{ old('no_of_guests') }}" placeholder="No. of Guests" min="50" max="1500" required>
                  <span class="glyphicon glyphicon-user form-control-feedback">
                  </span>

                  @if ($errors->has('no_of_guests'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_of_guests') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} has-feedback">
                  <label for="date">Pick A Date: </label>
                  <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" min="<?php echo date('Y-m-d');?>" required>
                  <span class="glyphicon glyphicon-calendar form-control-feedback">
                  </span>

                  @if ($errors->has('date'))
                    <span class="help-block">
                      <strong>{{ $errors->first('date') }}</strong>
                    </span>
                  @endif
                </div>
              
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary form-group">Add</button>
        </div>
      </div>
    </form>  
  </div>

@endsection
