<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\AclServiceProvider;
use App\Http\Controllers\UiController;
use Debugbar;

use App\Services\HomeDrawServiceProvider;

class HomeController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    |  Home Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for showing different views 
    | to the three types of users - Hosts, Merchants and Admin.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $service = new AclServiceProvider();
        $home = $service->Acl($request);
        return $home;
    }

    static function drawListView($params = array()){
        $options = array(
                    'cssClass' => 'modal-default',   // modal-sm, modal, modal-lg
                    'title' => 'Confirm Service Details:'
                );
        $data = UiController::drawModal('drawConfirmServiceModalBody','HomeController',$params,$options);

        return $data;
    }

    static function drawConfirmServiceModalBody($params = array()){
        
        return view('modalBody/modalBody', ['params'=>$params]);
    }

   /**
    * Function to draw the Modal structure for edit User profile.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawEditProfileForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeEditProfileModal($params);
    }

   /**
    * Function to draw the Modal body for User profile form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawEditProfileFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeEditProfileModalBody($params);
    }

    /**
    * Function to draw the Modal structure for viewing API token.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawMyApiTokenModal($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeMyApiTokenModal($params);
    }

   /**
    * Function to draw the Modal body for viewing API token.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawMyApiTokenModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeMyApiTokenModalBody($params);
    }

   /**
    * Function to draw the Modal structure for edit company profile.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawEditCompanyForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeEditCompanyModal($params);
    }

   /**
    * Function to draw the Modal body for edit company profile form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawEditCompanyFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeEditCompanyModalBody($params);
    }

   /**
    * Function to draw the Modal structure for Change Password Modal.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawChangePasswordForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeChangePasswordModal($params);
    }

   /**
    * Function to draw the Modal body for Change Password Modal form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawChangePasswordFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeChangePasswordModalBody($params);
    }

   /**
    * Function to draw the Modal structure for Google API settings.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawApiSettingForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeApiSettingModal($params);
    }

   /**
    * Function to draw the Modal body for Google API settings form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawApiSettingFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeApiSettingModalBody($params);
    }

   /**
    * Function to draw the Modal structure for Login.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawLoginForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeLoginModal($params);
    }

   /**
    * Function to draw the Modal body for Login form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawLoginFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeLoginModalBody($params);
    }

   /**
    * Function to draw the Modal structure for Register form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawRegisterForm($params = array())
    {
    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeRegisterModal($params);
    }

   /**
    * Function to draw the Modal body for Register form.
    *
    * @param  $params = array()
    * @return \Illuminate\Http\Response
    */
    static function drawRegisterFormModalBody($params = array())
    {

    $drawModal = new HomeDrawServiceProvider();
    return $drawModal->makeRegisterModalBody($params);
    }

}
