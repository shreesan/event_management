<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlinePayment extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Online Payment Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "online_payment" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'online_payments';

    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'description', 'events_booking_id', 'reciept_url', 'epoch_time',
    ];

    /**
     * Get the Event of the OnlinePayment.
     * The Foreign key names are overridden.
     */
    public function paidForEvent()
    {
        return $this->hasOne('App\Event', 'events_booking_id', 'booking_id');
    }
}
