@extends('layouts.mylayout')

@section('navigation')
	
@endsection

@section('sidebar')
  @parent
  <li>
    <a href="{{ url('/home') }}">
      <i class="fa fa-home"></i> Home
    </a>
  </li>
  
  <li class="header">
    <i class="fa fa-folder-open"></i> RESOURCES
  </li>

  <li class="active">
    <a href="{{ url('/myServices') }}">
      <i class="fa fa-check"></i> 
        <span> View My Services</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/pendingRequests') }}">
      <i class="fa fa-download"></i>
        <span class="hidden-xs"> Pending Requests</span>
    </a>
  </li>

@endsection

@section('content')
  <!-- View the Merchants Added Services -->
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>My <b>Services</b></h2>
        </div>
        <div class="col-sm-6">
          <!-- Open the modal to add services -->
          <a href="javascript:void(0);" class="btn btn-success btn-lg" onclick="openAddServicesModal('drawServiceAddForm', 'MerchantController', 'Add Service Details')">
            <i class="fa fa-plus"></i><span><strong>Add Service</strong></span>
          </a>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>&nbsp;</th>
          <th>Item Name</th>
          <th>Cost per service</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($items as $item)
          <tr>
            <td>
              <img id="merchantViewImage" src="images/{{ $item->image }}" alt="{{ $item->image }}">
            </td>
            <td>{{ $item->item }}</td>
            <td>&#8377 {{ $item->cost }} /-</td>
            <td>{{ $item->description }}</td>
            <td>

              <a href="javascript:void(0);" class="edit" onclick="openEditServicesModal('drawServiceEditForm', 'MerchantController', '{{ $item->id }}', 'Update Service Details')">
                <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
              </a>

              <!-- <a href="javascript:void(0);" class="delete" onclick="openDeleteServicesModal('drawServiceDeleteForm', 'MerchantController', '{{ $item->id }}', 'Delete your Service')">
                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
              </a> -->

              <a href="javascript:void(0);" class="delete" onclick="merchantService.deleteItem(this);" data-id="{{ $item->id }}" data-item="{{ $item->item }}">
                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
              </a>

            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
