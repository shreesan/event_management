<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class ApiAuthController extends Controller
{
	/*
    |--------------------------------------------------------------------------
    |  API Auth Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for guarenteeing that API calls are authosized.
    |
    */

    /**
     * Test function.
     *
     * @return \Illuminate\Http\Response
     */
	public function testApi(Request $request)
    {
        // $option = $_GET['option'];
        $option = $request->input('option');


		if ( $option == '1' ) {
			$data = ['Sending Event Data'];
		    // $data = [ 'Welcome', 'to', 'API' ];
		    // will encode to JSON array: ["a","b","c"]
		    // accessed as example in JavaScript like: result[1] (returns "b")
		} else {
			$data = ['Sending Venue Data'];
		    // $data = [ 'name' => 'Shreesan', 'age' => 23 ];
		    // will encode to JSON object: {"name":"God","age":-1}  
		    // accessed as example in JavaScript like: result.name or result['name'] (returns "God")
		}

		header('Content-type: application/json');

		// echo json_encode( $data );
		return response()->json($data);
		// return json_encode( $data );
	}

	/**
     * Get user_type_id of the requesting user.
     *
     * @param  int  $type, string $api_token
     * @return int  $userType
     */
	private function getUserType($type, $api_token){
		$userType = DB::table('users')
		->where('users.user_type_id', '=', $type)
		->where('users.api_token', '=', $api_token)
		->select('users.user_type_id')
		->pluck('user_type_id')
		->first();

	    return $userType;
	}

	/**
     * Get Event details for the requesting Host.
     *
     * @param  string  $api_token
     * @return array $events
     */
	private function getEvents($api_token){
		$events = DB::table('events')
	            ->join('users', 'events.user_id', '=', 'users.id')
	            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')

	            ->where('users.api_token', '=', $api_token)
	            ->where('events.bill_status', '=', 1)

	            ->select(
	                        'event_type.event_type',
	                        'events.date'
	                    )
	            ->get()
	            ->toArray();

	    return $events;
	}

	/**
     * Get Venue details for the requesting User.
     *
     * @return array $venues
     */
	private function getVenues(){
		$type = config('constants.hallOwner');

		$venues = DB::table('service_items')
        ->join('merchants', 'service_items.merchant_id', '=', 'merchants.id')
      
        ->where('merchants.merchant_type_id', '=', $type)
      
        ->select(
                    'service_items.item'
              	)
        ->get()
        ->toArray();

	    return $venues;
	}

	/**
     * Get and return the requested resources for the API call.
     *
     * @return \Illuminate\Http\Response
     */
	public function apiGetData(Request $request)
	{
		$api_token  = $request->api_token;
		$request_type  = $request->request_type;
		header('Content-type: application/json');

		if ($request_type) {
			if ( $request_type == 'event' ) {
				
				$type = config('constants.host');
				$userType = $this->getUserType($type, $api_token);

		        if ($userType == $type) {
		        	$data = $this->getEvents($api_token);
		        }
		        else {
		        	$data = ['Only Possible for Hosts'];
		        }
			}
			elseif ($request_type == 'venue') {
				$data = $this->getVenues();
			} 
			else {
				$data = ['Please send the correct option'];
			}
			return response()->json($data);
		}

		else {
			$data = ['No Option entered'];
			return response()->json($data);
		}

	}

}
