<?php

namespace App\Services;

use Illuminate\Http\Request;

use Auth; //to use the Auth facade in the controller
use DB;
use Debugbar;

use App\Http\Controllers\UiController;

/*
|--------------------------------------------------------------------------
|  Home Draw Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for drawing the Modal and its bodies for all 
| modals used commonly from the basic layout. 
|
*/
class HomeDrawServiceProvider
{
 /**
  * Function to draw the required Modal for Edit Profile form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeEditProfileModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawEditProfileFormModalBody','HomeController',$params,$options);

    return $data;
  }

 /**
  * Function to draw the Modal body for Edit Profile form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeEditProfileModalBody($params)
  {
  return view('modalBody/editProfileFormModalBody');
  }

  /**
  * Function to draw the required Modal for My API Token.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeMyApiTokenModal($params)
  {

    $title = $params['title'];
    
    $options = array(
                'cssClass' => '',   // modal-sm, modal, modal-lg
                'title' => $title
            );
    $data = UiController::drawModal('drawMyApiTokenModalBody','HomeController',$params,$options);

    return $data;
  }

 /**
  * Function to draw the Modal body for My API Token.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeMyApiTokenModalBody($params)
  {
  return view('modalBody/myApiTokenModalBody');
  }

 /**
  * Function to draw the required Modal for Edit Company Profile form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeEditCompanyModal($params)
  {

  $title = $params['title'];

  $options = array(
              'cssClass' => '',   // modal-sm, modal, modal-lg
              'title' => $title
          );
  $data = UiController::drawModal('drawEditCompanyFormModalBody','HomeController',$params,$options);

  return $data;
  }

 /**
  * Function to draw the Modal body for Edit Company Profile form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeEditCompanyModalBody($params)
  {
  return view('modalBody/editCompanyFormModalBody');
  }

 /**
  * Function to draw the required Modal for Change Password form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeChangePasswordModal($params)
  {

  $title = $params['title'];

  $options = array(
              'cssClass' => '',   // modal-sm, modal, modal-lg
              'title' => $title
          );
  $data = UiController::drawModal('drawChangePasswordFormModalBody','HomeController',$params,$options);

  return $data;
  }

 /**
  * Function to draw the Modal body for Change Password form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeChangePasswordModalBody($params)
  {
  return view('modalBody/changePasswordFormModalBody');
  }

 /**
  * Function to draw the required Modal for Google API settings form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeApiSettingModal($params)
  {

  $title = $params['title'];

  $options = array(
              'cssClass' => '',   // modal-sm, modal, modal-lg
              'title' => $title
          );
  $data = UiController::drawModal('drawApiSettingFormModalBody','HomeController',$params,$options);

  return $data;
  }

 /**
  * Function to draw the Modal body for Google API settings form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeApiSettingModalBody($params)
  {
  return view('modalBody/apiSettingModalBody');
  }

 /**
  * Function to draw the required Modal for Login form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeLoginModal($params)
  {

  $title = $params['title'];

  $options = array(
              'cssClass' => '',   // modal-sm, modal, modal-lg
              'title' => $title
          );
  $data = UiController::drawModal('drawLoginFormModalBody','HomeController',$params,$options);

  return $data;
  }

 /**
  * Function to draw the Modal body for Login form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeLoginModalBody($params)
  {
  return view('modalBody/loginFormModalBody');
  }

  /**
  * Function to draw the required Modal for Register form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeRegisterModal($params)
  {

  $title = $params['title'];

  $options = array(
              'cssClass' => '',   // modal-sm, modal, modal-lg
              'title' => $title
          );
  $data = UiController::drawModal('drawRegisterFormModalBody','HomeController',$params,$options);

  return $data;
  }

 /**
  * Function to draw the Modal body for Register form.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeRegisterModalBody($params)
  {
  return view('modalBody/registerFormModalBody');
  }

}