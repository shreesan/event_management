<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\User;

use App\Services\MerchantDetailServiceProvider;
use App\Services\AdminEventServiceProvider;
use App\Services\UserDetailServiceProvider;

class AdminController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    |  Admin Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling all requests to view, edit and   
    | delete all major data (Data of events and users as done by the Admin.
    |
    */

   /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Function to check for Authentication of the Admin.
     *
     * @return \Illuminate\Http\Response
     */
    private function checkAdmin($request)
    {
        $admin = config('constants.admin');
        $user = Auth::user()->user_type_id;
        
        //Check for the Admin.
        if ($user === $admin) {
            return 1;
        }
        else {
            abort(404,'Unauthorized Access !!!');
        }
    }

    /**
     * Show the merchantDetails page.
     *
     * @return \Illuminate\Http\Response
     */
    public function showMerchant(Request $request)
    {
        if ($this->checkAdmin($request) == 1) {
            $merchantDetail = new MerchantDetailServiceProvider();
            return $merchantDetail->showMerchantDetails($request);
        }
    }

    /**
     * To update and delete merchants.
     *
     * @return \Illuminate\Http\Response
     */
    public function merchantDetail(Request $request)
    {
        $input = $request->all();
        $merchantDetail = new MerchantDetailServiceProvider();

        switch ($input['operation']) {
            case 'editMerchant':
                return $merchantDetail->updateMerchantDetail($input);
                break;

            case 'delete':
                return $merchantDetail->deleteMerchantDetail($input);
                break;
        }
    }

    /**
     * Show the bookedEvents page with events and its related services in cart.
     *
     * @return \Illuminate\Http\Response
     */
    public function showEvents(Request $request)
    {
        if ($this->checkAdmin($request) == 1) {
            $adminEventDetail = new AdminEventServiceProvider();
            return $adminEventDetail->showEventDetails($request);
        }
    }

    /**

     * To update and delete Events.
     *
     * @return \Illuminate\Http\Response
     */
    public function eventDetail(Request $request)
    {
        $input = $request->all();
        $adminEventDetail = new AdminEventServiceProvider();

        switch ($input['operation']) {
            case 'editEvent':
                return $adminEventDetail->updateEventDetail($input);
                break;

            case 'delete':
                return $adminEventDetail->deleteEventDetail($input);
                break;

            case 'serviceDelete':
                return $adminEventDetail->deleteServiceDetail($input);
                break;
        }
    }


    /**
     * To update and delete users.
     *
     * @return \Illuminate\Http\Response
     */
    public function userDetail(Request $request)
    {
        $input = $request->all();
        $userDetail = new UserDetailServiceProvider();
        $merchantDetail = new MerchantDetailServiceProvider();

        switch ($input['operation']) {
            case 'editUser':
                return $userDetail->updateUserDetail($input);
                break;

            case 'changePassword':
                return $userDetail->changePassword($input);
                break;

            case 'editCompany':
                return $merchantDetail->updateMerchantDetail($input);
                break;

            case 'delete':
                return $userDetail->deleteUserDetail($input);
                break;
        }

    }

   /**
    * Set the Venue Location page.
    *
    * @return \Illuminate\Http\Response
    */
    public function setLocationOnMap(Request $request)
    {
        // if ($this->checkAdmin($request) == 1) {
            $adminEventDetail = new AdminEventServiceProvider();
            return $adminEventDetail->showVenueLocation($request);
        // }
    }

}
