<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Merchant;

class RegisterTest extends TestCase
{

	use WithFaker;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUserRegistration()
    {
        /*$user = factory(User::class)->create([
        	'name' => 'Test User Register',
            'password' => bcrypt($password = 'ImTesting'),
            'user_type_id' => '2',
        ]);
        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $response->assertRedirect('/home');
        $this->assertAuthenticatedAs($user);*/

        $user = factory(User::class)->create([
        	'name' => 'Test User Register',
            'password' => bcrypt($password = 'ImTesting'),
            'user_type_id' => '3',
        ]);

        $merchant = factory(Merchant::class)->create([
        	'user_id' => $user->id,
        	'merchant_type_id' => '1',
        ]);

        $response = $this->post('/register', [
        	'name' => $user->name,
			'user_type_id' => $user->user_type_id,
            'email' => $user->email,
            'address' => $user->address,
            'contact_no' => $user->contact_no,
            'password' => $password,

            'merchant_type_id' => $merchant->merchant_type_id,
            'company_name' => $merchant->company_name,
            'company_address' => $merchant->company_address,
            'landline_no' => $merchant->landline_no,
        ]);

        $response->assertRedirect('/');

        /*$user = [
			'name' => $this->faker->name,
			'user_type_id' => '3',
            'email' => $this->faker->unique()->safeEmail,
            'address' => $this->faker->address,
            'contact_no' => mt_rand(1000000000, 9999999999),
            'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm',
		];

		$merchant = [
			'merchant_type_id' => '1',
            'company_name' => $this->faker->company,
            'company_address' => $this->faker->city,
            'landline_no' => mt_rand(10000000000, 99999999999),
		];

		$array = array_merge($user, $merchant);
		fwrite(STDERR, print_r($array));

		$response = $this->post('/register', $array);
	    $response->assertRedirect('/');*/
    }
}
