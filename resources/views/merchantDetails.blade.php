@extends('layouts.mylayout')

@section('navigation')
	
@endsection

@section('sidebar')
  @parent
    <li>
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>
    
    <li class="header">
      <i class="fa fa-folder-open"></i> RESOURCES
    </li>

    <li class="active">
      <a href="{{ url('merchantDetails') }}">
        <i class="fa fa-user"></i> 
        <span> View Merchant Details</span>
      </a>
    </li>

    <li>
      <a href="{{ url('bookedEvents') }}">
        <i class="fa fa-pencil-square-o"></i> 
        <span> View Booked Events</span>
      </a>
    </li>
  
@endsection

@section('content')
  <!-- The Admin gets to see, edit, and delete merchant details here. -->
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2>Manage <b>Merchants</b></h2>
        </div>
      </div>
    </div>
    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Managed By</th>
          <th>Merchant Type</th>
          <th>Company Name</th>
          <th>Company Address</th>
          <th>Landline No.</th>
          <th>Actions</th>
        </tr>
      </thead>

      <tbody>
      	@foreach ($merchants as $merchant)
	        <tr>
            <td>{{ $merchant->id }}</td>
            <td>{{ $merchant->name }}</td>
            <td>{{ $merchant->merchant_type }}</td>

            <td>{{ $merchant->company_name }}</td>
            <td>{{ $merchant->company_address }}</td>
            <td>{{ $merchant->landline_no }}</td>
            <td>
              
              <a href="#modal-edit-{{ $merchant->id }}" class="edit" data-toggle="modal" data-backdrop="static" data-keyboard="false">
                <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i>
              </a>

              <!-- Edit Merchant Modal -->
              <div class="modal fade" id="modal-edit-{{ $merchant->id }}">
                <form method="POST" action="{{ url('/merchantDetails') }}">
                  {{ csrf_field() }}
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title text-center">Edit Merchant Data</h4>
                      </div>
                      <div class="modal-body">
                        <div class="login-box-body row">
                          <div class="col-xs-1"></div>

                          <div class="col-xs-10">
                            <div class="form-group">
                              <input id="operation" type="hidden" class="form-control" name="operation" value="editMerchant" required>
                            </div>

                            <div class="form-group">
                              <input id="id" type="hidden" class="form-control" name="id" value="{{ $merchant->id }}" required>
                            </div>

                            <div class="form-group">
                              <label for="merchant_type_id">Select Merchant Type: </label>
                              <select class="form-control" name="merchant_type_id" id="merchant_type_id">

                                <option value="1" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 1 ? 'selected' : '' }}>Caterer</option>

                                <option value="2" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 2 ? 'selected' : '' }}>Hall Owner</option>

                                <option value="3" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 3 ? 'selected' : '' }}>Decorator</option>

                                <option value="4" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 4 ? 'selected' : '' }}>Florist</option>

                                <option value="5" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 5 ? 'selected' : '' }}>Pandit</option>

                                <option value="6" required {{ old("merchant_type_id", $merchant->merchant_type_id) == 6 ? 'selected' : '' }}>Baker</option>

                              </select>
                            </div>

                            <div class="form-group{{ $errors->has('company_name') ? ' has-error' : '' }} has-feedback">
                              <label for="company_name">Enter Company Name: </label>
                              <input id="company_name" type="text" class="form-control" name="company_name" value="{{ $merchant->company_name }}" placeholder="Company Name" required autofocus>
                              <span class="glyphicon glyphicon-globe form-control-feedback"></span>

                              @if ($errors->has('company_name'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('company_name') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('company_address') ? ' has-error' : '' }} has-feedback">
                              <label for="company_address">Enter Company Address: </label>
                              <textarea id="company_address" type="text" class="form-control" name="company_address" rows="2" placeholder="Company Address" required>{{ $merchant->company_address }}</textarea>
                              <span class="glyphicon glyphicon-pushpin form-control-feedback"></span>

                              @if ($errors->has('company_address'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('company_address') }}</strong>
                                </span>
                              @endif
                            </div>

                            <div class="form-group{{ $errors->has('landline_no') ? ' has-error' : '' }} has-feedback">
                              <label for="landline_no">Enter Landline Number: </label>
                              <input id="landline_no" type="text" class="form-control" name="landline_no" value="{{ $merchant->landline_no }}" placeholder="Landline Number" required>
                              <span class="glyphicon glyphicon-phone-alt form-control-feedback"></span>

                              @if ($errors->has('landline_no'))
                                <span class="help-block">
                                  <strong>{{ $errors->first('landline_no') }}</strong>
                                </span>
                              @endif
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="modal-footer">
                        <button type="submit" class="btn btn-primary form-group">Update</button>
                      </div>

                    </div>
                  </div>
                </form>  
              </div>

              <a href="javascript:void(0);" class="delete" onclick="admin.deleteMerchant(this);" data-id="{{ $merchant->id }}" data-company_name="{{ $merchant->company_name }}" data-user_id="{{ $merchant->user_id }}">
                <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
              </a>
              
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>

@endsection
