<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- For OpenStreet Maps and Leaflet.JS -->
	  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
	  integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
	  crossorigin=""/>

	  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
	  integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
	  crossorigin=""></script>
  </head>

  <body>
  	<div id="mapid" style="height: 380px;"></div>

		<script>
			var venueName = "{{ $venueName }}";
			var venueAddress = "{{ $location }}";
			var coordinates = new Array(2);

			switch (venueAddress) {
			  case "Saheed Nagar":
			    coordinates = [20.288706, 85.844314];
			    break;
			  case "Brajabandhu Kalyan Mandap, Vivekenand Marg, BBSR":
			    coordinates = [20.245408, 85.835785];
			    break;
			  default:
			    text = "No Location found";
			}
			
			var mymap = L.map('mapid').setView(coordinates, 15);

			L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
		      maxZoom: 18,
		      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
		        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
		        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
		      id: 'mapbox.streets'
		    }).addTo(mymap);

		    L.marker(coordinates).addTo(mymap)
		      .bindPopup("<h3>"+venueName+"</h3><p>"+venueAddress+"</p>").openPopup();

		    var popup = L.popup();

		    function onMapClick(e) {
		      popup
		        .setLatLng(e.latlng)
		        .setContent("You clicked the map at " + e.latlng.toString())
		        .openOn(mymap);
		    }

		    mymap.on('click', onMapClick);

		</script>
  </body>
</html>
