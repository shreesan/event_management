<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Celebrations</title>
  <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- First loaded jQuery and then popper, follwed by bootstrap, to use BOOTBOX. -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="{{ asset('js/popper.min.js') }}"></script>

  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.min.css">
  <link rel="stylesheet" href="../bower_components/fullcalendar/dist/fullcalendar.print.min.css" media="print">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
  <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">

  <link href="{{ asset('css/design.css') }}" rel="stylesheet">
  <link href="{{ asset('css/layout.css') }}" rel="stylesheet">

  <!-- For OpenStreet Maps and Leaflet.JS -->
  <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
  integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
  crossorigin=""/>

  <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
  integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
  crossorigin=""></script>


  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

  <script src="{{ asset('js/checkbox.js') }}"></script>
  <script src="{{ asset('js/calendar.js') }}"></script>

  <script src="{{ asset('js/modal.js') }}"></script>
  <script src="{{ asset('js/merchant.js') }}"></script>
  <script src="{{ asset('js/admin.js') }}"></script>
  <script src="{{ asset('js/user.js') }}"></script>
  <script src="{{ asset('js/services.js') }}"></script>
  <script src="{{ asset('js/script.js') }}"></script>

</head>

<body class="hold-transition skin-red sidebar-mini">

  <div>
    <header class="main-header" id="myHeader">
      <a href="{{ url('/home') }}" class="logo">
        <img src="dist/img/sss.jpg" class="img-fluid" id="myLogo">
        <span id="logoTitle"> Celebrations</span>
      </a>

      <nav class="navbar navbar-fixed-top">
        <div class="navbar-custom-menu">
          <!-- Show login and register option for logged-out / new users -->
          <ul class="nav navbar-nav">
            @guest
              <li>
                <a href="javascript:void(0);" onclick="openLoginModal('drawLoginForm', 'HomeController', 'Sign in to start your session')">Login
                </a>

              </li>

              <li>
                <a href="javascript:void(0);" onclick="openRegisterModal('drawRegisterForm', 'HomeController', 'Register a new membership')">Register
                </a>
                
              </li>
            @else
              
              <li>
                @yield('navigation')
              </li>

              <!-- Show My Events for Hosts = type '2' user. -->
              @if (Auth::user()->user_type_id == 2)
                <li>
                  <a href="{{ url('/myEvents') }}">
                    <i class="fa fa-pencil-square-o"></i> 
                    <span> My Events </span>
                  </a>
                </li>
              @endif

              <!-- Show the following to the logged in users -->
              <!-- welcome message -->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>Welcome, 
                    {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <ul class="dropdown-menu">
                  <!-- Logout option -->
                  <li>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                  </li>

                  <!-- Show my profile to all users, which is editable. -->
                  <li>
                    <a href="javascript:void(0);" onclick="openEditProfileModal('drawEditProfileForm', 'HomeController', 'Edit Profile')">My Profile
                    </a>

                  </li>

                  <li>
                    <a href="javascript:void(0);" onclick="openMyApiTokenModal('drawMyApiTokenModal', 'HomeController', 'API Token')">My API Token
                    </a>

                  </li>

                  <!-- Show google API settings to Hosts = type '2' user. -->
                  @if (Auth::user()->user_type_id == 2)
                      <li>
                        <a href="javascript:void(0);" onclick="openApiSettingModal('drawApiSettingForm', 'HomeController', 'Google API Settings')">Google API Settings
                        </a>
                      </li>
                  @endif

                  <!-- Show the Company profile to Merchants = type '3' user. -->
                  @if (Auth::user()->user_type_id == 3)
                    <li>
                      <a href="javascript:void(0);" onclick="openEditCompanyModal('drawEditCompanyForm', 'HomeController', 'Edit Company Profile')">Company Profile
                      </a>

                    </li>
                  @endif

                  <!-- Show change password option to all logged-in users. -->
                  <li>
                    <a href="javascript:void(0);" onclick="openChangePasswordModal('drawChangePasswordForm', 'HomeController', 'Change Your Password')">Change Password
                    </a>
                  </li>

                </ul>
              </li>
            @endguest
          </ul>
        </div>
      </nav>

    </header>

    <aside class="main-sidebar" id="mySide">
      <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          @section('sidebar')
              <!-- This is the master sidebar. -->
          @show
        </ul>
      </section>
    </aside>

    <div class="content-wrapper" id="myContent">
      <section class="content-header">
        @yield('first_content')
      </section>

      <div id="app">
        @include('utilities/flash-message')
        @yield('flash_content')
      </div>

      <section class="content">
        @yield('content')
      </section>

    </div>

    <footer class="text-center" id="myFooter">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0.0
      </div>
      <strong>Copyright © 2019-2020 <a href="#">Shreesan Studio</a>.</strong> All rights reserved.
    </footer>
  </div>

<!-- Space for importing modals -->
  <div id="siteModalDiv"></div>

  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <script src="{{ asset('js/script.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="dist/js/pages/dashboard.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <!-- bootstrap datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- fullCalendar -->
  <script src="../bower_components/moment/moment.js"></script>
  <script src="../bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->

  <script type="text/javascript">
    var AJAX_URL = "{{ Config::get('constants.AJAX_URL') }}";
  </script>

  <script src="{{ asset('js/bootbox.min.js') }}"></script>
  <script src="{{ asset('js/bootbox.locales.min.js') }}"></script>

</body>
</html>
