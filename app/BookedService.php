<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookedService extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Booked Service Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "booked_services" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booked_services';

    /**
     * The attributes that are mass assignable. 
     *
     * @var array
     */
    protected $fillable = [
        'service_id', 'status', 'events_booking_id',
    ];

    /**
     * Get the ServiceItem of the BookedService.
     * The Foreign key names are overridden.
     */
    public function getServiceItem()
    {
        return $this->belongsTo('App\ServiceItem', 'service_id');
    }

    /**
     * Get the Event of the BookedService.
     * The Foreign key names are overridden.
     */
    public function bookedByEvent()
    {
        // return $this->belongsTo('App\Event', 'events_booking_id'); //original
        return $this->belongsTo('App\Event', 'events_booking_id', 'booking_id');
    }
}
