<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Event Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "events" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'event_type_id', 'payment_mode_id', 'date', 'is_complete', 'booking_id', 'no_of_guests', 'bill_status','bill_amount','google_status', 'payment_status',
    ];

    /**
     * Get the EventType of the Event.
     * The Foreign key names are overridden.
     */
    public function eventType()
    {
        return $this->belongsTo('App\EventType', 'event_type_id');
    }

    /**
     * Get the PaymentMode of the Event.
     * The Foreign key names are overridden.
     */
    public function payType()
    {
        return $this->belongsTo('App\PaymentMode', 'payment_mode_id');
    }

    /**
     * Get the User of the Event.
     * The Foreign key names are overridden.
     */
    public function getEventUser()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * Get the BookedServices of the Event.
     * The Foreign key names are overridden.
     */
    public function hasBookedServices()
    {
        return $this->hasMany('App\BookedService', 'events_booking_id');
    }

    /**
     * Get the OnlinePayment of the Event.
     * The Foreign key names are overridden.
     */
    public function hasOnlinePayment()
    {
        return $this->belongsTo('App\OnlinePayment', 'events_booking_id');
    }
}
