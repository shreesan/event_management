@foreach ($item as $item)
  <div class="login-box-body row">
    <div class="col-xs-1"></div>

    <div class="col-xs-10">
      <!-- <form id="editServiceForm" enctype="multipart/form-data" method="POST" action="{{ url('/myServices') }}"> -->
    	<form id="editServiceForm" enctype="multipart/form-data">
			{{ csrf_field() }}
        <div class="form-group">
        <input id="operation" type="hidden" class="form-control" name="operation" value="edit" required>
        </div>

        <div class="form-group">
          <input id="id" type="hidden" class="form-control" name="id" value="{{ $item->id }}" required>
        </div>

        <div class="form-group{{ $errors->has('item') ? ' has-error' : '' }} has-feedback">
          <input id="item" type="text" class="form-control" name="item" value="{{ $item->item }}" placeholder="Name of the Service" required autofocus>
          <span class="glyphicon glyphicon-shopping-cart form-control-feedback"></span>

          @if ($errors->has('item'))
            <span class="help-block">
              <strong>{{ $errors->first('item') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('cost') ? ' has-error' : '' }} has-feedback">
          <input id="cost" type="number" class="form-control" name="cost" value="{{ $item->cost }}" placeholder="Cost" min="10" max="30000" required>
          <span class="glyphicon glyphicon-credit-card form-control-feedback">
          </span>

          @if ($errors->has('cost'))
            <span class="help-block">
              <strong>{{ $errors->first('cost') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }} has-feedback">
          <textarea id="description" type="text" class="form-control" name="description" rows="6" placeholder="Description" required>{{ $item->description }}</textarea>
          <span class="glyphicon glyphicon-edit form-control-feedback"></span>

          @if ($errors->has('description'))
            <span class="help-block">
              <strong>{{ $errors->first('description') }}</strong>
            </span>
          @endif
        </div>

        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }} has-feedback">
          <input id="image" type="file" class="form-control" name="image" value="{{ $item->image }}" required>{{ old('image') }}
          <span class="glyphicon glyphicon-picture form-control-feedback"></span>

          @if ($errors->has('image'))
            <span class="help-block">
              <strong>{{ $errors->first('image') }}</strong>
            </span>
          @endif

          @if($item->image)
            <img src="images/{{ $item->image }}" alt="{{ $item->image }}" height=70 >
          @endif
        </div>

        <!-- <button type="submit" class="btn btn-primary form-group">Update</button> -->
        <!-- <input id="submit" onclick="formSubmit()" type="button" value="Submit" class="btn btn-primary form-group"> -->

        <!-- now : -->
        <!-- <div class="form-group pull-right">
          <button class="btn btn-success btn-submit">Submit</button>
        </div> -->

      </form>
    </div>

  </div>
@endforeach	      
