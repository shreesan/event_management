<form method="POST" action="{{ url('/home') }}">
	{{ csrf_field() }}

  <div class="form-group">
    <input id="operation" type="hidden" class="form-control" name="operation" value="editUser" required>
  </div>

  <div class="form-group">
    <input id="id" type="hidden" class="form-control" name="id" value="{{ Auth::user()->id }}" required>
  </div>

  <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }} has-feedback">
    <label for="name">Enter Full name: </label>
    <input id="name" type="text" class="form-control" name="name" value="{{ Auth::user()->name }}" placeholder="Full name" required autofocus>
    <span class="glyphicon glyphicon-user form-control-feedback"></span>

    @if ($errors->has('name'))
      <span class="help-block">
        <strong>{{ $errors->first('name') }}</strong>
      </span>
    @endif
  </div>

  <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback">
    <label for="email">Enter email: </label>
    <input id="email" type="email" class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="Email" required>
    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>

    @if ($errors->has('email'))
      <span class="help-block">
        <strong>{{ $errors->first('email') }}</strong>
      </span>
    @endif
  </div>

  <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }} has-feedback">
    <label for="address">Enter Address: </label>
    <textarea id="address" type="text" class="form-control" name="address" rows="2" placeholder="Personal Address" required>{{ Auth::user()->address }}</textarea>
    <span class="glyphicon glyphicon-home form-control-feedback"></span>

    @if ($errors->has('address'))
      <span class="help-block">
        <strong>{{ $errors->first('address') }}</strong>
      </span>
    @endif
  </div>

  <div class="form-group{{ $errors->has('contact_no') ? ' has-error' : '' }} has-feedback">
    <label for="contact_no">Enter Contact Number: </label>
    <input id="contact_no" type="text" class="form-control" name="contact_no" value="{{ Auth::user()->contact_no }}" placeholder="Contact Number" required>
    <span class="glyphicon glyphicon-phone form-control-feedback"></span>

    @if ($errors->has('contact_no'))
      <span class="help-block">
        <strong>{{ $errors->first('contact_no') }}</strong>
      </span>
    @endif
  </div>

  <div class="form-group pull-right">
  	<button type="submit" class="btn btn-primary form-group">Update</button>
  </div>

</form>
