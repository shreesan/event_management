@section('event_content')
  <!-- To show the particular type of available services for an event. -->
  @foreach ($services as $service)
  	<div class="col-md-4">
      <div class="box box-widget">
        <div class="box-header with-border">
          <div class="user-block">
            <span class="username">{{ $service->item }}</span>
          </div>
        </div>
        <div class="box-body row">
          <div class="col-md-1"></div>
          <div class="col-md-10" id="serviceBox">
            <img class="img-responsive pad" src="images/{{ $service->image }}" alt="{{ $service->image }}" id="serviceImage">
            
            <div id="descriptionBox">
              <p>{{ $service->description }}</p>
            </div>
            <br>

            <p><STRONG>Provided by : </STRONG>{{ $service->company_name }}</p>
            
            <p><STRONG>Cost : </STRONG> &#8377 {{ $service->cost }} /-</p>

            @if ($type == config('constants.hallOwner'))
              <a href="javascript:void(0);" class="btn btn-primary pull-left" onclick="openLocationModal('drawVenueLocationModal', 'EventServiceController', '{{ $service->item }}', '{{ $service->company_address }}', 'Location Of The Venue')">View Location
              </a>
              <!-- <a href="{{ url('/venueLocation') }}" target="_blank" class="btn btn-primary pull-left" >View Location</a> --> 
            @endif

            <a class="btn btn-primary pull-right" role="button" data-toggle="modal" data-target="#modal-cart-{{ $service->id }}" data-backdrop="static" data-keyboard="false">Add To Cart</a>
            
            <div class="modal fade" id="modal-cart-{{ $service->id }}">
              <form action="{{ url('/addToCart') }}" method="post">
              {{ csrf_field() }}
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      <h4 class="modal-title text-center">Select Event for the Requested Service: </h4>
                    </div>
                    <!-- This Modal shows the upcoming - unbilled events for which the services are booked. -->
                    <div class="modal-body">
                      <div class="login-box-body row">
                        <div class="col-xs-1"></div>

                        <div class="col-xs-10">
                        @if (count($events) > 0 )
                          <div class="form-group">
                            @foreach ($events as $event)
                              <div class="radio">
                                <label>
                                  <input type="radio" name="events_booking_id" id="events_booking_id" value="{{ $event->booking_id }}" required>
                                    <strong>{{ $event->event_type }}</strong> on {{ date('jS \of F Y , l', strtotime($event->date)) }}
                                </label>
                              </div>
                            @endforeach
                          </div>
                          
                          <div class="form-group">
                            <input id="service_id" type="hidden" class="form-control" name="service_id" value="{{ $service->id }}" required>
                          </div>
                          
                        @else
                          <!-- Add an event just on that modal, when there is no event, to book a service for. -->
                          <h4 class="text-center">No Events to add services for.<br><br>Kindly add an Event below.</h4>
                        @endif

                        </div>
                      </div>
                    </div>

                    <div class="modal-footer">
                      @if ( count($events) > 0 )
                        <a href="#" class="btn btn-primary pull-left btn-xs" data-toggle="modal" data-target="#modal-event" data-backdrop="static" data-keyboard="false">
                          <i class="fa fa-gift"></i> Add Another Event
                        </a>
                        <button type="submit" class="btn btn-primary form-group">Submit</button>
                      @else
                        <a href="#" class="btn btn-primary pull-left btn-xs" data-toggle="modal" data-target="#modal-event" data-backdrop="static" data-keyboard="false">
                          <i class="fa fa-gift"></i> Add Event
                        </a>
                        <button class="btn btn-primary form-group" disabled="disabled" data-toggle="tooltip" title="No Events to Add Service For">Submit</button>
                      @endif
                    </div>

                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

  <!-- Add Events Modal -->
  <div class="modal fade" id="modal-event">
    <form method="POST" action="{{ url('/myEvents') }}">
      {{ csrf_field() }}
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title text-center">Add Your Event</h4>
          </div>
          <div class="modal-body">
            <div class="login-box-body row">
              <div class="col-xs-1"></div>

              <div class="col-xs-10">

                <div class="form-group">
                  <input id="operation" type="hidden" class="form-control" name="operation" value="add" required>
                </div>

                <div class="form-group">
                  <label for="event_type_id">Select Event Type: </label>
                  <select class="form-control" name="event_type_id" id="event_type_id">
                    <option value="1" required>Marriage</option>
                    <option value="2">Birthday</option>
                    <option value="3">Anniversary</option>
                    <option value="4">Grihapravesh</option>
                    <option value="5">Thread Ceremony</option>
                    <option value="6">Annaprasanna</option>
                  </select>
                </div>

                <div class="form-group">
                  <label for="payment_mode_id">Select Payment Mode: </label>
                  <select class="form-control" name="payment_mode_id" id="payment_mode_id">
                    <option value="1" required>Cash</option>
                    <option value="2">Cheque</option>
                    <option value="3">Online Transfer</option>
                  </select>
                </div>

                <div class="form-group{{ $errors->has('no_of_guests') ? ' has-error' : '' }} has-feedback">
                  <label for="no_of_guests">Number Of Guests: </label>
                  <input id="no_of_guests" type="number" class="form-control" name="no_of_guests" value="{{ old('no_of_guests') }}" placeholder="No. of Guests" min="50" required>
                  <span class="glyphicon glyphicon-user form-control-feedback">
                  </span>

                  @if ($errors->has('no_of_guests'))
                    <span class="help-block">
                      <strong>{{ $errors->first('no_of_guests') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }} has-feedback">
                  <label for="date">Pick A Date: </label>
                  <input id="date" type="date" class="form-control" name="date" value="{{ old('date') }}" min="<?php echo date('Y-m-d');?>" required>
                  <span class="glyphicon glyphicon-calendar form-control-feedback">
                  </span>

                  @if ($errors->has('date'))
                    <span class="help-block">
                      <strong>{{ $errors->first('date') }}</strong>
                    </span>
                  @endif
                </div>
              
              </div>
            </div>
          </div>
        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary form-group">Add</button>
        </div>
      </div>
    </form>  
  </div>

@endsection