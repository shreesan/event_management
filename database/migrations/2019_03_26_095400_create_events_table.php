<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('booking_id')->unique();

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->integer('event_type_id')->unsigned();
            $table->foreign('event_type_id')
            ->references('id')
            ->on('event_type')
            ->onDelete('cascade');

            $table->integer('payment_mode_id')->unsigned();
            $table->foreign('payment_mode_id')
            ->references('id')
            ->on('payment_mode')
            ->onDelete('cascade');
            
            $table->timestamp('date');
            $table->tinyInteger('no_of_guests');
            $table->tinyInteger('payment_type');
            $table->tinyInteger('is_complete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
