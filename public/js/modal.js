var Modal = {
  create: function(functionName,className,postData,callBack,requestType){
    callBack  = callBack || function(){};
    postData  = postData || {};
    requestType = requestType || 'POST';

    postData.className    = className;
    postData.functionName   = functionName;

    var onComplete = function(response){
      // show the modal
      $('#siteModalDiv').html(response);
      $('#siteModalDiv #genericModal').addClass('in').css({'display':'block','padding-right':'15px'});

      // any other necessary stuffs - which are general in nature
      callBack(response);
    }

    $.ajax({
    	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    },
      url: AJAX_URL,
      type: requestType,
      data: postData,
      // dataType: 'html',
      success: function(response){
        onComplete(response);
      },
      error: function(response){
        console.log('Error'+response);
      }
    });
  },
  
  close: function(){
  	// close the modal or destroy
  	$('#siteModalDiv #genericModal').remove();
  },

  // function to add button in the modal footer, with required functionality.
  addFooterBtn: function(buttonName, className, onClick){
    buttonName = buttonName || 'save';
    className = className || "btn-default";
    className = 'pull-right btn '+className;
    onClick   = onClick || function(){};

    var button = document.createElement('button');

    button.type = 'button'; 
    button.className = className;
    button.innerHTML = buttonName;
    button.onclick = onClick;

    document.getElementById('footerBtnArea').appendChild(button);

    /*$('div.modal-footer').append(
      "<button type='button' type='submit' class='pull-right btn btn-submit "+className+"' onclick='"+onClick+"'>"+buttonName+"</button>"
    );*/
  },
  confirmBox: function(title,msg,actionBtn,action){
    title     = title || 'Delete Record';
    msg       = msg || 'Are you sure?<br >This can not be undone.';
    actionBtn = actionBtn || 'Confirm';
    action    = action || function(){};
    
    bootbox.confirm({
      title: '<span style="font-weight: bold;font-size: 20px;">'+title+'</span>',
      message: msg,
      buttons: {
          cancel: {
              label: '<i class="fa fa-times"></i> Cancel'
          },
          confirm: {
              label: '<i class="fa fa-check"></i>'+actionBtn,
              className: 'btn-danger'
          }
      },
      callback: function (result) {
        if (result == true) {
          action();
        }
        else {
          console.log(result);
        }
      }
    });
  },
  alertBox: function(response){
    bootbox.alert({
      message: '<span style="font-weight: bold;font-size: 15px;">'+response+'</span>',
      size: 'small',
      callback: function () {
        location.reload();
      }
    });
  },
  loaderOpen: function(){
    bootbox.dialog({
      size: 'small',
      message: '<p class="text-center" style="font-weight: bold;font-size: 15px;"><i class="fa fa-spin fa-spinner"></i> Please Wait...</p>',
      closeButton: false,
    });
  },
  bootboxClose: function(){
    bootbox.hideAll();
  }
};
