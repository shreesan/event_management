<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\Services\EventItemServiceProvider;

class EventServiceController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  |  Event Service Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for viewing all the event services pages 
  | to the Hosts - Show halls, catering, florist, etc service pages.
  |
  */

  /**
  * Create a new controller instance.
  *
  * @return void
  */
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  /**
   * Function to check for Authentication of the Host.
   *
   * @return \Illuminate\Http\Response
   */
  private function checkHost($request)
  {
    $host = config('constants.host');
    $user = Auth::user()->user_type_id;

    //Check for the Host.
    if ($user === $host) {
      return 1;
    }
    else {
      abort(404,'Unauthorized Access !!!');
      // return 0;
    }
  }

  /**
   * Show the halls page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showHalls(Request $request)
  {
    $hallOwner = config('constants.hallOwner');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($hallOwner, 'services/halls');
    }
    else {
      // return redirect('/home')->with('warning','Unauthorized Access');
    }
  }


  /**
   * Show the Florists page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showFlorist(Request $request)
  {
    $florist = config('constants.florist');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($florist, 'services/florist');
    }
  }


  /**
   * Show the decoration page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showDecoration(Request $request)
  {
    $decorator = config('constants.decorator');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($decorator, 'services/decoration');
    }
  }

  /**
   * Show the catering page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showCatering(Request $request)
  {
    $caterer = config('constants.caterer');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($caterer, 'services/catering');
    }
  }

  /**
   * Show the pandits page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showPandit(Request $request)
  {
    $pandit = config('constants.pandit');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($pandit, 'services/pandit');
    }
  }

  /**
   * Show the bakers page.
   *
   * @return \Illuminate\Http\Response
   */
  public function showBaker(Request $request)
  {
    $baker = config('constants.baker');
    if ($this->checkHost($request) == 1) {
      $itemService = new EventItemServiceProvider();
      return $itemService->viewService($baker, 'services/baker');
    }
  }

  /**
  * Function to draw the Modal structure for Venue Location.
  *
  * @param  $params = array()
  * @return \Illuminate\Http\Response
  */
  static function drawVenueLocationModal($params = array())
  {
    $drawModal = new EventItemServiceProvider();
    return $drawModal->makeVenueLocation($params);
  }

  /**
  * Function to draw the Modal body for Venue Location.
  *
  * @param  $params = array()
  * @return \Illuminate\Http\Response
  */
  static function drawVenueLocationModalBody($params = array())
  {
    $drawModal = new EventItemServiceProvider();
    return $drawModal->makeVenueLocationBody($params);
  }


}
