var merchantService = {
  deleteItem: function(el){
    el    = $(el);
    id    = el.data('id');
    item  = el.data('item');
    title = 'Delete Service';
    msg   = 'Are you sure you want to delete "<strong>'+item+'</strong>" from your added services ?<br ><p class="text-warning"><small>This action cannot be undone.</small></p>';

    params = {};
    params.operation = 'delete';
    params.id = id;

    Modal.confirmBox(title, msg, 'Delete', function(){
      Modal.loaderOpen();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/myServices',
        type: 'POST',
        data: params,
        success: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        },
        error: function(response){
          Modal.bootboxClose();
          Modal.alertBox(response);
        }
      });
    });
  }
};

function openConfirmModal(functionName, className, id) {
  Modal.create(functionName, className, {type:1,name:"Shreesan",userType:3,id:id},function(r){
    console.log(r)
  });
}

function openConfirmServicesModal(functionName, className, event_id, title) {
  Modal.create(functionName, className, {event_id:event_id, title:title},function(r){
    console.log(r)
  });
}

function openAddServicesModal(functionName, className, title) {
  Modal.create(functionName, className, {title:title},function(r){
    Modal.addFooterBtn('Add', 'btn-primary',function(){
      document.getElementById("addServiceForm").submit();
    });
  });
}

function openEditServicesModal(functionName, className, item_id, title) {
  Modal.create(functionName, className, {item_id:item_id, title:title},function(r){
    Modal.addFooterBtn('Update', 'btn-success',function(){

      /*var formData = new FormData($('#editServiceForm'));
      console.log(formData);*/

      // var formData = new FormData($('form')[0]);
      var formData = new FormData($('editServiceForm')[0]);
      console.log(formData);

      $.ajax({
        url: '/myServices',
        type: 'POST',
        data: new FormData($('form')[0]),
        cache: false,
        contentType: false,
        processData: false,

        xhr: function () {
          var myXhr = $.ajaxSettings.xhr();
          if (myXhr.upload) {
            // For handling the progress of the upload
            myXhr.upload.addEventListener('progress', function (e) {
              if (e.lengthComputable) {
                $('progress').attr({
                  value: e.loaded,
                  max: e.total,
                });
              }
            }, false);
          }
          return myXhr;
        }
      });


      /*document.getElementById("editServiceForm").submit(function(event){
        var formData = new FormData(this);
        console.log(formData);
        $.ajax({
            url: '/myServices',
            type: 'POST',
            dataType: 'JSON',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data, status) {
            },
            error: function (xhr, desc, err) {
            }
        });
      });*/



      // document.getElementById("editServiceForm").submit();
      
      /*var formData = $('#editServiceForm').serialize();
      console.log(formData); 

      Modal.close();
      Modal.loaderOpen();

      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '/myServices',
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          Modal.bootboxClose();
          Modal.alertBox(response);
        },
        error: function(response) {
          Modal.bootboxClose();
          Modal.alertBox(response);
        }
      });*/

      
    });
  });
}

function openDeleteServicesModal(functionName, className, item_id, title) {
  Modal.create(functionName, className, {item_id:item_id, title:title},function(r){
    console.log(r)
  });
}

function openAcceptRequestModal(functionName, className, cart_id, title) {
  Modal.create(functionName, className, {cart_id:cart_id, title:title},function(r){
    console.log(r)
  });
}

function openRejectRequestModal(functionName, className, cart_id, title) {
  Modal.create(functionName, className, {cart_id:cart_id, title:title},function(r){
    console.log(r)
  });
}


$("#idOfEditForm").submit(function (event) {
    event.preventDefault();
    var formData = $(this).serialize();
    console.log(formData);

    $.ajax({
        url: '/myServices',
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log(data);
            // alert(data);
        },
        error: function(data){
            console.log(data);
            // alert(data);
        }
    });

    return false;
});
