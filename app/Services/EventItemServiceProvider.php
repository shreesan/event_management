<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use Validator;
use DB;
use Debugbar;

use App\ServiceItem;
use App\Event;
use App\Merchant;

use App\Http\Controllers\UiController;

/*
|--------------------------------------------------------------------------
|  Event Item Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for showing respective items available, as per item type. 
|
*/
class EventItemServiceProvider
{
  /**
   * Function to return service items to respective views.
   *
   * @param int $type, string $view
   * @return \Illuminate\Http\Response
   */
  public function viewService($type, $view)
  {
    $services = DB::table('service_items')
          ->join('merchants', 'service_items.merchant_id', '=', 'merchants.id')
          
          ->where('merchants.merchant_type_id', '=', $type)
          
          ->select(
                    'service_items.id',
                    'service_items.item',
                    'service_items.cost',
                    'service_items.image',
                    'merchants.company_name',
                    'merchants.company_address',
                    'service_items.description'
                  )
          ->get();

    $id = Auth::user()->id;

    $events = DB::table('events')
            ->join('users', 'events.user_id', '=', 'users.id')
            ->join('event_type', 'events.event_type_id', '=', 'event_type.id')

            ->where('users.id', '=', $id)
            ->where('events.is_complete', '=', 0)
            ->where('events.bill_status', '=', 0)
            
            ->select(
                      'events.booking_id',
                      'event_type.event_type',
                      'events.date'
                    )
            ->get();
    
    // sending $type so as to check whether the view location button to be shown
    return view($view, ['services'=>$services, 'events'=>$events, 'type'=> $type]);
  }

  /**
   * Show items added by merchant.
   *
   * @return \Illuminate\Http\Response
   */
  public function merchantServices($request)
  {
    $id = Auth::user()->id;

    $items = DB::table('service_items')
          ->join('merchants', 'service_items.merchant_id', '=', 'merchants.id')
          
          ->where('merchants.user_id', '=', $id)
          
          ->select(
                    'service_items.id',
                    'service_items.item',
                    'service_items.cost',
                    'service_items.image',
                    'service_items.description'
                  )
          ->get();

      return view('services/myServices', ['items'=>$items]);
  }

  /**
   * Add a new Service details.
   *
   * @param  array  $input
   * @return \Illuminate\Http\Response
   */
  public function addServiceDetail($request, $input)
  {
    $this->validator($input)->validate();

    $user = Auth::user()->id;
    $merchant_id = Merchant::where('user_id', $user)->first()->id;

    /* Store the image in its original extention, in public folder. */
    $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
    $request->image->move(public_path('images'), $input['image']);

    $serviceAdded = ServiceItem::create([
        'merchant_id' => $merchant_id,
        'item' => $input['item'],
        'cost' => $input['cost'],
        'description' => $input['description'],
        'image' => $input['image'],
    ]);

    if ($serviceAdded) {
      return back()->with('success','Item added successfully!');
    }
    return back()->with('error','Oops! Something Went Wrong.');
  }

  /**
   * Update the Service details.
   *
   * @param  array  $input
   * @return \Illuminate\Http\Response
   */
  public function updateServiceDetail($request, $input)
  {
    Debugbar::log('Inside Updates service function');
    Debugbar::info($request);
    $this->validator($input)->validate();

    $id = $input['id'];

    $input['image'] = time().'.'.$request->image->getClientOriginalExtension();
    $request->image->move(public_path('images'), $input['image']);

    $serviceUpdated = ServiceItem::where('id', $id)
        ->update([
            'item' => $input['item'],
            'cost' => $input['cost'],
            'description' => $input['description'],
            'image' => $input['image'],
        ]);

    if ($serviceUpdated) {
      // return back()->with('success','Service Updated Successfully!');
      return response()->json('Service Updated Successfully!');
    }
    // return back()->with('error','Oops! Something Went Wrong.');
    return response()->json('Oops! Something Went Wrong.');

  }

  /**
   * Delete the Service.
   *
   * @param  array  $input
   * @return \Illuminate\Http\Response
   */
  public function deleteServiceDetail($input)
  {
    $id = $input['id'];
    $serviceDeleted = ServiceItem::where('id', $id)->delete();
    
    if ($serviceDeleted) {
      return response()->json('Service Deleted successfully!');
      // return back()->with('success','Service Deleted successfully!');
    }
    // return back()->with('error','Oops! Something Went Wrong.');
    return response()->json('Oops! Something Went Wrong.');
  }


  /**
   * Get a validator for an incoming add service request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'item' => 'required|string|max:50',
      'cost' => 'required|numeric|min:10|max:30000',
      'description' => 'required|string|max:255',
      // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048', //Validations for the Image to be uploaded by the merchant.
    ]);
  }

  /**
  * Function to draw the required Modal for Venue Location.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeVenueLocation($params)
  {

    $title = $params['title'];
    $location = $params['address'];
    $venueName = $params['venueName'];
    
    $options = array(
                'cssClass' => 'modal-lg',   // modal-sm, modal, modal-lg
                'title' => $title,
                'location' => $location,
                'venueName' => $venueName
            );
    $data = UiController::drawModal('drawVenueLocationModalBody','EventServiceController',$params,$options);

    return $data;
  }

  /**
  * Function to draw the Modal body for Venue Location.
  *
  * @param  $params
  * @return \Illuminate\Http\Response
  */
  static function makeVenueLocationBody($params)
  {
    $location = $params['address'];
    $venueName = $params['venueName'];
    return view('modalBody/venueLocationModalBody', ['location' => $location], ['venueName' => $venueName]);
  }

}