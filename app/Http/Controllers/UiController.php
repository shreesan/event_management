<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Debugbar;

class UiController extends Controller
{
  static function drawModal($functionName, $className, $params=array(), $options=array()){
  	$className		= !empty($className)?$className:'';
  	$functionName	= !empty($functionName)?$functionName:'';
  	$options['id']	= (isset($options['id']) && $options['id'])?$options['id']:'genericModal';
  	$options['cssClass']	= (isset($options['cssClass']) && $options['cssClass'])?$options['cssClass']:'modal-default';
  	$options['title']	= (isset($options['title']) && $options['title'])?$options['title']:'Modal Title';

  	if(!$className || !$functionName){
  		Debugbar::log('ERROR: Missing required parameters.');
  		return false;
  	}

  	$options['body'] = call_user_func(array('App\Http\Controllers\\'.$className, $functionName),$params);

  	return response()->json([
      	view('utilities/modal')->with('options',$options)->render()
   	]);

  }
}