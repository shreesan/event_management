<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller
use App\BookedService;
use App\ServiceItem;

/*
|--------------------------------------------------------------------------
|  Cart Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for all interactions with the Booked services in Cart. 
|
*/
class CartServiceProvider
{
	/**
     * Function to add services to Cart.
     *
     * @return \Illuminate\Http\Response
     */
    public function cartAdd($request)
    {
        $hallOwner = config('constants.hallOwner');
                        
        $service_id = $request->service_id;
        $events_booking_id = $request->events_booking_id;

        $bookedServices = BookedService::where('events_booking_id', $events_booking_id)->get();

        $serviceItem = ServiceItem::where('id', $service_id)->get()->first();
        $serviceType = $serviceItem->getMerchant->merchantType->id;

        foreach ($bookedServices as $bookedService) {
            // Checking for whether the service already exists in the cart.
            if (($bookedService->service_id) == $service_id) {
                return back()->with('error','Service Already Exists in Cart !');
            }

            $serviceTypeInCart = $bookedService->getServiceItem->getMerchant->merchantType->id;

            // Condition to make sure that only one Hall/Mandap/Venue is added for each event.
            if (($serviceTypeInCart === $hallOwner) && ($serviceType === $hallOwner)) {
                return back()->with('error','Cannot add more than one Venue for the Event !');
            }
        }

        $addedToCart = BookedService::create([
            'service_id' => $service_id,
            'events_booking_id' => $events_booking_id,
            'status' => '0',
        ]);

        if ($addedToCart) {
            return back()->with('success','Service Added to Cart Successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }

    /**
     * Function to delete services from the Cart.
     *
     * @return \Illuminate\Http\Response
     */
    public function cartDelete($input)
    {
        $id = $input['id'];
        $deletedFromCart = BookedService::where('id', $id)->delete();
        
        if ($deletedFromCart) {
            return back()->with('success','Service Deleted successfully from Cart!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }
}