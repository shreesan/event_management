@extends('layouts.mylayout')

@section('navigation')
	
@endsection

@section('sidebar')
  @parent
    <li>
      <a href="{{ url('/home') }}">
        <i class="fa fa-home"></i> Home
      </a>
    </li>

    <li class="header">
      <i class="fa fa-gift"></i> SERVICES
    </li>

    <li>
      <a href="{{ url('/halls') }}">
        <i class="fa fa-bank"></i> Halls & Venues
      </a>
    </li>
    
    <li>
      <a href="{{ url('/decoration') }}">
        <i class="fa fa-snowflake-o"></i> Decorations
      </a>
    </li>
    
    <li class="active">
      <a href="{{ url('/catering') }}">
        <i class="fa fa-cutlery"></i> Catering
      </a>
    </li>
    
    <li>
      <a href="{{ url('/florist') }}">
        <i class="fa fa-leaf"></i> Flowers and Garlands
      </a>
    </li>
    
    <li>
      <a href="{{ url('/pandit') }}">
        <i class="fa fa-bell"></i> Pujas and Rituals
      </a>
    </li>
    
    <li>
      <a href="{{ url('/baker') }}">
        <i class="fa fa-birthday-cake"></i> Bakery
      </a>
    </li>

    <li class="header">
      <i class="fa fa-folder-open"></i> RESOURCES
    </li>

    <li>
      <a href="{{ url('/myEvents') }}">
        <i class="fa fa-pencil-square-o"></i> 
        <span> My Events </span>
      </a>
    </li>

@endsection

@section('first_content')
  <div style="font-weight: bold; color: black;">
    <h2>Available Options for Food Items
      <small>We manage, you enjoy !</small>
    </h2>
  </div>
@endsection

@section('content')
<!-- View all available Catering Items -->
  @include('utilities/eventService')
  @yield('event_content') 
   
@endsection
