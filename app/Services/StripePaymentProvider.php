<?php

namespace App\Services;

use App\Http\Requests;
use Illuminate\Http\Request;
use Validator;

use App\User;
use App\Event;
use App\OnlinePayment;

use Stripe\Error\Card;
use Cartalyst\Stripe\Stripe;

/*
|--------------------------------------------------------------------------
|  Stripe Payment Provider
|--------------------------------------------------------------------------
|
| This Class is responsible for managing the online payment using Stripe API. 
|
*/
class StripePaymentProvider
{
	/**
     * Function to do the online payment, using Stripe.
     *
     * @return \Illuminate\Http\Response
     */
    public function payment($input)
    {
    	$this->validator($input)->validate();
        // echo "hello!";
        // dd($input);
        $stripeKey = config('constants.stripeKey');
        $currency = config('constants.currency');        

        $stripe = Stripe::make($stripeKey);
			
		try {
			$token = $stripe->tokens()->create([
						'card' => [
								'number' => $input['card_no'],
								'exp_month' => $input['ccExpiryMonth'],
								'exp_year' => $input['ccExpiryYear'],
								'cvc' => $input['cvvNumber'],
							],
					]);
			
			if (!isset($token['id'])) {
				return back()->with('Error','Please Try Again.');
			}

			$charge = $stripe->charges()->create([
						'card' => $token['id'],
						'currency' => $currency,
						'amount' => $input['bill_amount'],
						'description' => 'Payment recieved for '.$input['description'],
						'metadata' => (['event_booking_id' => $input['booking_id']]),
					]);

			if($charge['status'] == 'succeeded') {

				$metadata = $charge['metadata'];
				$event_booking_id = $metadata['event_booking_id'];
				
				OnlinePayment::create([
		            'transaction_id' => $charge['id'],
		            'events_booking_id' => $event_booking_id,
		            'description' => $charge['description'],
		            'reciept_url' => $charge['receipt_url'],
		            'epoch_time' => $charge['created'],
		        ]);

		        Event::where('booking_id', $event_booking_id)
		            ->update([
		                'payment_status' => '1',
		            ]);

				/*echo "<pre>";
				print_r($charge);exit();*/
				return back()->with('success','Payment Successful.');
			} 
			else {
				$error = $e->getMessage();
				return back()->with('error',$error);
			}
		} 
		catch (Exception $e) {
			$error = $e->getMessage();
			return back()->with('error',$error);
		} 
		catch(\Cartalyst\Stripe\Exception\CardErrorException $e) {
			$error = $e->getMessage();
			return back()->with('error',$error);
		} 
		catch(\Cartalyst\Stripe\Exception\MissingParameterException $e) {
			$error = $e->getMessage();
			return back()->with('error',$error);
		}
        
    }

    /**
     * Get a validator for an incoming payment card details.
     *
     * @param  array  $input
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $input)
    {
      return Validator::make($input, [
	        'card_no' => 'required|digits:16',
			'ccExpiryMonth' => 'required|digits:2|between:1,12',
			'ccExpiryYear' => 'required|digits:4',
			'cvvNumber' => 'required|digits:3',
		]);
    }
}
