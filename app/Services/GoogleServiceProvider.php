<?php

namespace App\Services;

use Illuminate\Http\Request;
use Auth; //to use the Auth facade in the controller

use App\Event;
use App\User;
use App\EventType;

// For Google API usage (Calendar) :
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;

use PDF; //To use DomPDF
use View;
use Debugbar; //To use Debugbar

/*
|--------------------------------------------------------------------------
|  Google Service Provider
|--------------------------------------------------------------------------
|
| This Class is responsible creating Google Client and adding Events to Calendar. 
|
*/
class GoogleServiceProvider
{
	/**
     * Creates a Google API client object.
     * 
     * @return New Google client object.
     */
    private function createGoogleClient()
    {
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json'); // Get Config information from the client_secrets file.
        $client->addScope(Google_Service_Calendar::CALENDAR); // To use all properties of Google Calendar API.
        return $client;
    }

    /**
     * Returns an authorized Google API client.
     * 
     * @param  array  $input
     * @return Google_Client the authorized client object
     * @return \Illuminate\Http\Response
     */
    public function getGoogleClient($input)
    {
        $id = $input['id'];

        $client = $this->createGoogleClient();
        $client->setAccessType("offline"); 
        /* offline access - Indicates if the application wants to access the user's account if the user is logged in or not */
        $client->authenticate($input['auth_code']);
        $access_token = $client->getAccessToken();

        // Must store the access_token info. in jason format.
        $token = json_encode($access_token);
        
        $googleAunthenticated = User::where('id', $id)
                ->update([
                    'google_auth_code' => $token,
                ]);

        if ($googleAunthenticated) {
          return back()->with('success','Google Authentication Done Successfully!');
        }
        return back()->with('error','Oops! Something Went Wrong.');
    }


    /**
     * Add Event to Google Calendar.
     *
     * @param  array  $input
     * @return \Illuminate\Http\Response
     */
    public function addToGoogleCalendar($input)
    {
        $id = $input['booking_id'];

        $timeZone = config('constants.timeZone'); // using timeZone constant declared in config/constants.php file.

        $event_type_id = $input['event_type_id'];
        $summary = EventType::where('id', $event_type_id)->get()->pluck('event_type');
        $start = $input['date'];

        $user = Auth::user()->id;
        $token = User::where('id', $user)->get()->pluck('google_auth_code')->first();

        $client = $this->createGoogleClient();
        $client->setAccessToken($token);

        $service = new Google_Service_Calendar($client);

        /* Store the required information in the below array format before inserting into the Google Calendar. For a fullday event, pass the same value for start and end dates. */
        $event = new Google_Service_Calendar_Event(array(
          'summary' => $summary,
          'start' => array(
            'date' => $start,
            'timeZone' => $timeZone,
          ),
          'end' => array(
            'date' => $start,
            'timeZone' => $timeZone,
          ),
        ));

        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);

        if (!$event) {
            return back()->with('error','Oops! Something Went Wrong.');
        }
        else {
            Event::where('booking_id', $id)
            ->update([
                'google_status' => '1', // Update in database that the event has been added to google calendar.
            ]);
            return back()->with('success','Google Event Created Successfully!');
        }

    }
}