@extends('layouts.mylayout')

@section('navigation')
  
@endsection

@section('sidebar')
  @parent
  <li class="active">
    <a href="{{ url('/home') }}">
      <i class="fa fa-home"></i> Home
    </a>
  </li>
  
  <li class="header">
    <i class="fa fa-folder-open"></i> RESOURCES
  </li>

  <li>
    <a href="{{ url('/myServices') }}">
      <i class="fa fa-check"></i> 
        <span> View My Services</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/pendingRequests') }}">
      <i class="fa fa-download"></i>
        <span class="hidden-xs"> Pending Requests</span>
    </a>
  </li>
  

@endsection

@section('content')

@if (count($myEvents) > 0)
  <div class="table-wrapper">
    <div class="table-title">
      <div class="row">
        <div class="col-sm-6">
          <h2><b>Events</b> to be served</h2>
        </div>
      </div>
    </div>

    <table class="table table-striped table-hover">
      <thead>
        <tr>
          <th>Booking ID</th>
          <th>Booked By</th>
          <th>Event Type</th>
          <th>Date</th>
          <th>No. of Guests</th>
          <th>Payment Mode</th>
          <th>Status</th>
          <th>Services</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($myEvents as $event)
          <tr>
              <td>{{ $event->booking_id }}</td>
              <td>{{ $event->name }}</td>
              <td>{{ $event->event_type }}</td>
              <td>{{ date('jS \of F Y , l', strtotime($event->date)) }}</td>
              <td>{{ $event->no_of_guests }}</td>
              <td>{{ $event->payment_mode }}</td>

              @if (($event->is_complete) == 0)
                <td>Upcoming</td>
              @else
                <td>Completed</td>
              @endif

              <td>
                
                <?php 
                  $title = "Services to be provided for <strong>".$event->event_type."</strong> on ".date('jS \of F Y , l', strtotime($event->date)).", booked by <strong>".$event->name."</strong>";
                ?>

                <!-- <button onclick="openConfirmServicesModal('drawListView', 'MerchantController', '{{ $event->booking_id }}', '{{ $title }}')">
                  <i class="material-icons" data-toggle="tooltip" title="View Cart">shopping_cart</i>
                </button> -->
                
                <a href="#" class="edit" onclick="openConfirmServicesModal('drawListView', 'MerchantController', '{{ $event->booking_id }}', '{{ $title }}')">
                  <i class="material-icons" data-toggle="tooltip" title="View Cart">shopping_cart</i>
                </a>

              </td>
              
          </tr>
        @endforeach
      </tbody>
    </table>
  </div>
@else
  <h1 class="text-center">No Service Bookings Yet.</h1>
@endif


<!-- <div>
  @foreach ($myEvents as $event)
    <p>{{ $event->booking_id }} 

      <button onclick="openConfirmModal('drawListView', 'HomeController', '{{ $event->booking_id}}')">Click</button>

    </p>
        
  @endforeach

</div> -->


@endsection
