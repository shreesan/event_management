<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantType extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  Merchant Type Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "merchant_type" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'merchant_type';

    /**
     * Get the Merchants for the MerchantType.
     * The Foreign key names are overridden.
     */
    public function merchants()
    {
        return $this->hasMany('App\Merchant', 'merchant_type_id');
    }
}
