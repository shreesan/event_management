# run - java -jar -Dwebdriver.chrome.driver="/usr/local/share/chromedriver" /usr/local/share/selenium/selenium-server-standalone-3.7.1.jar >/dev/null 2>&1 & 
# To initiate Selenium2 jar before your test suites

Feature:
    In order to prove that Behat works as intended
    We want to test the home page for a phrase

Scenario: Welcome Page test
    When I am on the homepage
    Then I should see "Welcome to Celebrations.com"

@javascript
Scenario: Registration form test
	Given I am on the homepage
	When  I follow "Register"
	Then  I should see "Register a new membership"
	Then  I should see "I already have a membership"

	Then I fill in the following
	| name | email | address | contact_no | password | password-confirm |
	| sona | test@example.com | jagatpur | 8989892222 | password123 | password123 |
	And I select "2" from "user_type_id"

	When I press "Register"
	# Then show last response
	Then I should see "Welcome to Celebrations.com"

@javascript 
Scenario: Login form test
	Given I am on the homepage
	When  I follow "Login"
	Then  I should see "Sign in to start your session"

	# Then I fill in the following
	Given the following user details:
	| email | password |
	| tanushree@gmail.com | monamaa |

	# Then print last response
	When I press "Login"
	# Then show last response
	# Then print current URL
	Then I should see "Welcome to Celebrations.com"

@javascript @eventspage
Scenario:Inside host home
    Given I am on "http://127.0.0.1:8000/myEvents"
    # When  I follow "My Events"
    Then I should see "My Events" in the "" element
