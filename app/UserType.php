<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserType extends Model
{
    /*
    |--------------------------------------------------------------------------
    |  User Type Model
    |--------------------------------------------------------------------------
    |
    | This Model is for the "user_type" table and its relationships. 
    |
    */

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_type';

    /**
     * Get the Users for the UserType.
     * The Foreign key names are overridden.
     */
    public function users()
    {
        return $this->hasMany('App\User', 'user_type_id');
    }
}
